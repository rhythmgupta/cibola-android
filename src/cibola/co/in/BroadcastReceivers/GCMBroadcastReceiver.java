/**
 *	GCMBroadcastReceiver.java
 *  Created by Rhythm Gupta on 04-Sep-2014, 2:24:13 PM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.BroadcastReceivers;

import cibola.co.in.NetworkManager.Services.GCMService;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class GCMBroadcastReceiver extends WakefulBroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		ComponentName comp = new ComponentName(context.getPackageName(),
                GCMService.class.getName());
		System.out.println("Some notification rcvd.");
		startWakefulService(context, intent.setComponent(comp));
		setResultCode(Activity.RESULT_OK);
	}
	
}
