
package cibola.co.in.UI;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.UI.Fragments.DialogContactUs;

public class ContactUs extends ActionBarActivity{

	public static boolean isDialogOpen = false;
	private DialogContactUs dialog; 	
	@Override
	public void onCreate(Bundle savedInstanceState){
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.fragment_contact_us);
		TextView title = (TextView) findViewById(R.id.drawer_fragment_title);
		TextView sub_title = (TextView) findViewById(R.id.drawer_fragment_subtitle);

		TextView text1 = (TextView) findViewById(R.id.contact_us_text1);
		TextView text2 = (TextView) findViewById(R.id.contact_us_text2);
		TextView text3 = (TextView) findViewById(R.id.contact_us_text3);
		TextView text4 = (TextView) findViewById(R.id.contact_us_text4);
		TextView text5 = (TextView) findViewById(R.id.contact_us_text5);
		TextView text6 = (TextView) findViewById(R.id.contact_us_text6);
		TextView text7 = (TextView) findViewById(R.id.contact_us_text7);
		
		TextView button_send = (TextView) findViewById(R.id.contact_us_send_message);

		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), title, sub_title, text1, text2,
				text3, text4,text5, text6, text7, button_send);
		Common.setupActionBar(getSupportActionBar());
		
		button_send.setOnClickListener(onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.contact_us_send_message:
				dialog = new DialogContactUs();
				dialog.show(getSupportFragmentManager());
				
				break;
			default:
				break;
			}

		}
	};

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed(){
		if(isDialogOpen){
			if(dialog!=null){
				dialog.dismiss();
			}
			else{
				super.onBackPressed();
				slideInLeft();
			}
		}
		else{
			super.onBackPressed();
			slideInLeft();
		}
	}
	
	public void slideInLeft(){
		overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_left);
	}
	
}
