package cibola.co.in.UI;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.DialogManager;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.Interfaces.CustomKeypadListener;
import cibola.co.in.NetworkManager.NetworkConnectionDetector;
import cibola.co.in.UI.Fragments.KeypadFragment;
import cibola.co.in.thirdparty.Paytm;

public class LoadMoney extends ActionBarActivity implements CustomKeypadListener{

	private MyAccountManager accountManager;
	private PhotoManager photoManager;

	private TextView amount;
	private TextView currBal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_load_money);
		photoManager = new PhotoManager();
		accountManager = new MyAccountManager(this);
		Common.setupActionBar(getSupportActionBar());
		amount = (TextView) findViewById(R.id.load_money_amount);
		ImageView profile_pic = (ImageView) findViewById(R.id.load_money_profile_pic);
		TextView name = (TextView) findViewById(R.id.load_money_name);
		currBal = (TextView) findViewById(R.id.load_money_current_bal);
		TextView text01 = (TextView) findViewById(R.id.load_money_current_bal_title);
		TextView text02 = (TextView) findViewById(R.id.load_money_add_amount);
		ImageView submit = (ImageView) findViewById(R.id.load_money_load_button);
		getSupportFragmentManager().beginTransaction().replace(R.id.load_money_keypad_group, new KeypadFragment()).commit();

		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), name, currBal, text01, text02);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_LIGHT, getAssets(), amount);

		profile_pic.setImageBitmap(photoManager.getSelfChatPic(this));
		name.setText(accountManager.getFullname());
		currBal.setText(accountManager.getBalance()+"");

		submit.setOnClickListener(onClickListener);

		setUpActionBar();

	}

	private void setUpActionBar(){
		ActionBar actionBar = getSupportActionBar();
		View view = this.getLayoutInflater().inflate(R.layout.actionbar_upload_money, null);
		TextView title = (TextView) view.findViewById(R.id.load_money_title);
		LinearLayout back = (LinearLayout) view.findViewById(R.id.load_money_back);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), title);
		actionBar.setCustomView(view);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);
		back.setOnClickListener(onClickListener);
	}


	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.load_money_load_button:
			{
				if(NetworkConnectionDetector.isConnected(LoadMoney.this)){
					String amt = amount.getText().toString();
					amt = amt.replaceAll(",", ""); 

					if(amt.length()>0){
						int money_load = Integer.parseInt(amt);
						Paytm paytm = new Paytm();
						paytm.execPaytmTrans(LoadMoney.this, money_load);
					}
					else{
						Common.showToast(LoadMoney.this, "Please enter a valid amount.");
					}
				}
				else{
					DialogManager.showInternetError(LoadMoney.this);
				}
				break;	
			}
			case R.id.load_money_back:
			{
				onBackPressed();
				break;
			}
			default:
				break;
			}
		}
	};

	@Override
	public void onEnteredValueChange(String currentValue) {
		amount.setText(Common.getCommaSepString(currentValue));

	}

	@Override
	public void onResume(){
		super.onResume();
		currBal.setText(accountManager.getBalance()+"");

	}

	@Override
	public void onBackPressed(){
		super.onBackPressed();
	}
}
