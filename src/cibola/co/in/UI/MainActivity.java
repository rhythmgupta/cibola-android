package cibola.co.in.UI;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.General.Raw;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.NetworkConnectionDetector;
import cibola.co.in.NetworkManager.RequestType;
import cibola.co.in.NetworkManager.Services.ContactsService;
import cibola.co.in.NetworkManager.Services.NetworkService;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.UI.Fragments.NavigationDrawerFragment;
import cibola.co.in.UI.Fragments.TransactionContactsFragment;

public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks,
WebConnectionCallbacks{

	//private MenuItem add;
	public static NavigationDrawerFragment mNavigationDrawerFragment;
	private static FragmentManager fManager;
	private MyAccountManager accountManager;
	//private MixpanelAPI mixpanel;
	private static boolean isSearchEnabled = false;
	private Database db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			Raw.raw(this, this);
		} catch (JSONException e) {
			e.printStackTrace();
		}


		setContentView(R.layout.activity_main);
		fManager = getSupportFragmentManager();
		mNavigationDrawerFragment = (NavigationDrawerFragment) fManager
				.findFragmentById(R.id.navigation_drawer);
		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));


		fManager
		.beginTransaction()
		.replace(R.id.container3,
				new TransactionContactsFragment()).commit();

		accountManager = new MyAccountManager(getApplicationContext());

		boolean fromNotification = getIntent().getBooleanExtra("notification", false);
		if(fromNotification){
			accountManager.setNotificationCount(0);
		}

		if(!accountManager.isInitialized()){
			accountManager.setIsInitialized(true);
		}
		if(NetworkConnectionDetector.isConnected(getApplicationContext())){
			if(!accountManager.isGCMRegisteredOnCiola()){
				Log.d("DEBUG","trying to save gcm id on server");
				if(accountManager.hasGCMId()){
					Intent intent = new Intent(this, NetworkService.class);
					intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_REGISTER_GCM_ON_CIBOLA);
					startService(intent);
				}
				else{
					Intent intent = new Intent(this, NetworkService.class);
					intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_GET_GCM);
					startService(intent);
				}
			}

			Intent intent = new Intent(this,ContactsService.class);
			intent.putExtra(Constants.CONTACTS_MANAGER_IS_INIT, 0);
			startService(intent);
		}
		
		

		//mixpanel = MixpanelAPI.getInstance(getApplicationContext(), Constants.MIXPANEL_TOKEN);
		//mixpanel.identify(accountManager.getMobileNumber());
		try{
			JSONObject props = new JSONObject();
			props.put("Gender", "Female");
			props.put("Plan", "Premium");
			//mixpanel.track("Plan Selected", props);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {
		//mixpanel.flush();
		super.onDestroy();
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		System.out.println("onNavigationDrawerItemSelected:- "+position);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		switch (position) {
		case 0:
			startActivity(new Intent(this, RefundToBank.class));
			overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_right);
			break;
		case 1:
			startActivity(new Intent(this, ManageAccounts.class));
			overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_right);
			break;
		case 2:
			startActivityForResult( new Intent(this, ManageProfile.class), 1);
			overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_right);
			break;
		case 3:
			startActivity(new Intent(this, ContactUs.class));
			overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_right);
			break;
		case 4:
			startActivity(new Intent(this, Privacy.class));
			overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_right);
			break;
		case 5:
			startActivity(new Intent(this, Help.class));
			overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_right);
			break;
		default:
			ft.replace(R.id.container3,
					new TransactionContactsFragment()).commit();
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(false);
		View view = this.getLayoutInflater().inflate(R.layout.actionbar_user_detail, null);
		TextView actionbarAmount = (TextView) view.findViewById(R.id.actionbar_amount);
		TextView actionbarName = (TextView) view.findViewById(R.id.actionbar_username);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), actionbarAmount, actionbarName);
		actionbarAmount.setText(accountManager.getBalance()+"");
		
		actionbarName.setText(Common.getAutoCapString(accountManager.getFirstname())+" "+
								Common.getAutoCapString(accountManager.getLastname()));
		actionBar.setCustomView(view);
		actionBar.setIcon(android.R.color.transparent);
		
		actionBar.setHomeAsUpIndicator(R.drawable.icon_drawer);
		actionBar.setDisplayShowCustomEnabled(true);
	}

	@Override
	public void onBackPressed() {
		isSearchEnabled = false;
		if(isSearchEnabled){
			isSearchEnabled = false;
			restoreActionBar();
			getSupportActionBar().setDisplayShowCustomEnabled(true);
			getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_drawer));
			return;
		}
		else{
			super.onBackPressed();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
			MenuItem searchItem = menu.findItem(R.id.action_search);
			SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
			// searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
			// Assumes current activity is the searchable activity
			searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
			// searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
			return true;
		}
		//add = menu.findItem(R.id.action_add_to_wallet);
		//search = menu.findItem(R.id.action_search);

		//		SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
		//	    searchView.setIconifiedByDefault(true);
		//	    getSupportActionBar().setCustomView(searchView);
		//	    ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
		//	    getSupportActionBar().getCustomView().setLayoutParams(params);
		//	    getSupportActionBar().setDisplayShowCustomEnabled(true);


		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		else if(id==R.id.action_search)
		{
			ActionBar actionBar = getSupportActionBar();
			//actionBar.setCustomView(R.layout.search);
			//			actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM| ActionBar.DISPLAY_SHOW_HOME);
			//			add.setVisible(false);

			//invalidateOptionsMenu();
			isSearchEnabled = true;

			//			Intent waIntent = new Intent(android.content.Intent.ACTION_SEND);
			//			waIntent.setType("text/plain");
			//			String text = "Hey! Check out Cibola at www.cibola.co.in ";
			//			waIntent.putExtra(Intent.EXTRA_TEXT, text);
			//			startActivity(Intent.createChooser(waIntent, "Share with"));
			return true;
		}
		else if(id==R.id.action_add_to_wallet){
			startActivity(new Intent(this, LoadMoney.class));
		}
		//		else if(id==R.id.action_add_to_wallet){
		//			execPaytmTrans();
		//		}
		return super.onOptionsItemSelected(item);
	}

	public class ProfileholderFragment extends Fragment {

		public RelativeLayout rootProfile;

		public ProfileholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_profile, container,
					false);
			TextView name = (TextView) rootView.findViewById(R.id.profile_name);
			TextView amount = (TextView) rootView.findViewById(R.id.profile_amount);
			rootProfile = (RelativeLayout) rootView.findViewById(R.id.fragment_profile_root);

			Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(), name,amount);
			return rootView;
		}
	}

	@Override
	public void onRequestComplete(RequestType requestType, String response) {
		Log.d("MainActivity: Response",response);

	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onResume(){
		super.onResume();
		restoreActionBar();
		
		mNavigationDrawerFragment.refreshBalance();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		System.out.println("requestCode: "+requestCode+", resultCode:"+resultCode);
		if (requestCode == 1) {
			if(resultCode == RESULT_OK){
				boolean result=data.getBooleanExtra("isPictureChanged", false);
				System.out.println("onActivityResult called: "+result);
				if(result){
					mNavigationDrawerFragment.refreshProfile();
				}
			}
		}
	}
}

