package cibola.co.in.UI;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.RequestType;
import cibola.co.in.UI.Fragments.SignupFragment;

public class SignUpActivity extends ActionBarActivity implements WebConnectionCallbacks{


	private SignupFragment fragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		fragment = new SignupFragment(this);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, fragment).commit();
		}
		getSupportActionBar().hide();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(!(resultCode == Activity.RESULT_OK)){
			Toast.makeText(this, "Some error occurred. Please try again", Toast.LENGTH_LONG).show();
			return;
		}
		if(requestCode==fragment.PICK_FROM_FILE_CROP){
			fragment.setProfileImage();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.sign_up, menu);
		return true;
	}

		@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onRequestComplete(RequestType requestType, String response) {
		Log.d("SignUpActivity Response",response);
		if(response==null){
			Common.showToast(getApplicationContext(), "Some error Occurred. Please try again.");
			return;
		}
		switch (requestType) {
		case RequestTypeSignUpStepThree:
			try{
				JSONObject jobj = new JSONObject(response);
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Intent i = new Intent(SignUpActivity.this, MobileVerifyActivity.class);
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}
				else{
					Common.showToast(getApplicationContext(), "Uh, Oh! Some error occurred. Please try again.");
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			break;
		default:
			break;
		}

	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
	}
	


}
