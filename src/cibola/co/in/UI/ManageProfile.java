
package cibola.co.in.UI;


import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cibola.co.in.General.CircularImageView;
import cibola.co.in.General.Common;
import cibola.co.in.General.CommunicationIntents;
import cibola.co.in.General.Constants;
import cibola.co.in.General.DialogManager;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Graphics.ImageBlur;
import cibola.co.in.Graphics.ImageCrop;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.Marketplace;
import cibola.co.in.NetworkManager.NetworkConnectionDetector;
import cibola.co.in.NetworkManager.RequestType;
import cibola.co.in.NetworkManager.Services.NetworkService;

public class ManageProfile extends ActionBarActivity implements WebConnectionCallbacks{

	private MyAccountManager accountManager;
	private PhotoManager photoManager;
	private Marketplace marketplace;
	private CommunicationIntents communicator;
	private CircularImageView profilePic ;
	private EditText name ;
	private EditText email ;
	private EditText old_pwd ;
	private EditText new_pwd ;
	private EditText confirm_pwd ;
	private boolean isPicChanged = false;
	private Pattern fullNamePattern = Pattern.compile("[a-zA-Z]+ [a-zA-Z]+");
	private Pattern emailPattern = Patterns.EMAIL_ADDRESS;
	
	private LinearLayout cover ;
	private int PICK_FROM_FILE_CROP = 101;
	private ImageBlur blur;
	@Override
	public void onCreate(Bundle savedInstanceState){
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.fragment_edit_profile);
		cover = (LinearLayout) findViewById(R.id.edit_profile_cover);
		findViewById(R.id.edit_profile_cover_overlay).getBackground()
		.setAlpha(Constants.PROFILE_COVER_OPACITY);
		
		TextView text1 = (TextView) findViewById(R.id.edit_profile_text1);
		TextView text2 = (TextView) findViewById(R.id.edit_profile_text2);
		TextView changePWD = (TextView) findViewById(R.id.edit_profile_change_pwd);
		TextView changePIN = (TextView) findViewById(R.id.edit_profile_change_pin);
		name = (EditText) findViewById(R.id.edit_profile_full_name);
		email = (EditText) findViewById(R.id.edit_profile_email);
		old_pwd = (EditText) findViewById(R.id.edit_profile_old_pwd);
		new_pwd = (EditText) findViewById(R.id.edit_profile_new_pwd);
		confirm_pwd = (EditText) findViewById(R.id.edit_profile_cnf_pwd);
		profilePic = (CircularImageView) findViewById(R.id.edit_profile_pic);
		
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getAssets(),text1,text2, changePIN, changePWD,
				name, email, old_pwd, new_pwd, confirm_pwd);
		Common.setupActionBar(getSupportActionBar());
		photoManager = new PhotoManager();
		accountManager = new MyAccountManager(getApplicationContext());
		marketplace = new Marketplace(this);
		communicator = new CommunicationIntents();
		blur = new ImageBlur();
		
		Bitmap bitmap = photoManager.getSelfImage();
		if(bitmap!=null){
			profilePic.setImageBitmap(bitmap);
			blur.setBlurredBackground(cover, bitmap);
		}
		
		profilePic.setOnClickListener(onClickListener);
		changePIN.setOnClickListener(onClickListener);
		changePWD.setOnClickListener(onClickListener);
		name.setText(accountManager.getFullname());
		email.setText(accountManager.getEmail());
		name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_edit, 0);
		email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_edit, 0);
//		name.setImeActionLabel("Save", R.id.edit_profile_save_name);
//		email.setImeActionLabel("Save", KeyEvent.KEYCODE_ENTER);
		name.setSaveEnabled(true);
		name.setOnKeyListener(keyListener);
		email.setOnKeyListener(keyListener);
		name.setOnFocusChangeListener(focusChangeListener);
		email.setOnFocusChangeListener(focusChangeListener);
		name.setCompoundDrawablePadding(10);
		email.setCompoundDrawablePadding(10);
		name.getBackground().setAlpha(Constants.PROFILE_COVER_OPACITY);
		email.getBackground().setAlpha(Constants.PROFILE_COVER_OPACITY);
	}
	
	private void selectProfileImage(){
		Log.d("DEBUG#1", "CALLED");
		ImageCrop crop = new ImageCrop();
		Intent cropImageIntent = crop.cropImage();
		startActivityForResult(cropImageIntent,PICK_FROM_FILE_CROP);
	}
	
	private void setProfileImage(){
		Bitmap bitmap = photoManager.getSelfImage();
		if(bitmap!=null){
			isPicChanged = true;
			profilePic.setImageBitmap(bitmap);
			Intent intent = new Intent(ManageProfile.this, NetworkService.class);
			intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_UPDATE_PIC);
			startService(intent);			
			ImageBlur blur = new ImageBlur();
			blur.setBlurredBackground(cover, bitmap);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(!(resultCode == Activity.RESULT_OK)){
			Toast.makeText(this, "Some error occurred. Please try again", Toast.LENGTH_LONG).show();
			return;
		}
		if(requestCode==PICK_FROM_FILE_CROP){
			setProfileImage();
			isPicChanged = true;
		}
	}
	
	
	private OnClickListener onClickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.edit_profile_pic:
			{
				if(NetworkConnectionDetector.isConnected(ManageProfile.this)){
					selectProfileImage();
				}
				else{
					DialogManager.showInternetError(ManageProfile.this);
				}
				
				break;
			}
			case R.id.edit_profile_change_pin:
				Common.showToast(ManageProfile.this, "Under construction!");
				break;
			case R.id.edit_profile_change_pwd:
				String old =  old_pwd.getText().toString();
				String newPass = new_pwd.getText().toString();
				String confirmPass = confirm_pwd.getText().toString();
				if(old.length()>0 && newPass.length()>0 && confirmPass.length()>0){
					if(newPass.equals(confirmPass)){
						marketplace.updatePassword(ManageProfile.this, old, newPass);
					}
					else{
						Common.showToast(ManageProfile.this, "New password mismatch. Please confirm your new password");
					}
				}
				else{
					Common.showToast(ManageProfile.this, "Please input all the 3 fields.");
				}
				break;
			default:
				break;
			}
			
		}
	};

	private OnFocusChangeListener focusChangeListener = new OnFocusChangeListener() {
		
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if(hasFocus){
				
				switch (v.getId()) {
				case R.id.edit_profile_full_name:
					name.setCursorVisible(true);
					name.setEnabled(true);
					//name.setCompoundDrawables(null, null, null, null);
					break;
				case R.id.edit_profile_email:
					
					email.setCursorVisible(true);
					email.setEnabled(true);
					//email.setCompoundDrawables(null, null, null, null);
					break;
				default:
					break;
				}
			}
			else{
				switch (v.getId()) {
				case R.id.edit_profile_full_name:
					//name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_edit, 0);
					break;
				case R.id.edit_profile_email:
					//email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.icon_edit, 0);
					break;
				default:
					break;
				}
			}
			
		}
	};
	
	
	private OnKeyListener keyListener = new OnKeyListener() {
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if ((event.getAction() == KeyEvent.ACTION_UP) &&
					(keyCode == KeyEvent.KEYCODE_ENTER)){
				switch (v.getId()) {
				case R.id.edit_profile_full_name:
				{
					
					String _fullname = name.getText().toString();
					if(fullNamePattern.matcher(_fullname).matches()){
						name.clearFocus();
						name.setCursorVisible(false);
						Common.hideKeyBoardFromEditText((EditText)v, getApplicationContext());
						String _firstname = _fullname.split(" ")[0];
						String _lastname = _fullname.split(" ")[1];
						if(!accountManager.getFirstname().equals(_firstname) || !accountManager.getLastname().equals(_lastname)){
							Intent intent = new Intent(ManageProfile.this, NetworkService.class);
							communicator.setUpdateNameIntent(intent, _firstname, _lastname);
							startService(intent);	
						}
						
						return true;
					}
					else{
						Common.showToast(ManageProfile.this, "Please input your firstname and lastname both.");
						return false;
					}
					
				}
				case R.id.edit_profile_email:
				{
					String _email  = email.getText().toString();
					if(emailPattern.matcher(_email).matches()){
						email.clearFocus();
						email.setCursorVisible(false);
						Common.hideKeyBoardFromEditText((EditText)v, getApplicationContext());
						if(!accountManager.getEmail().equals(_email)){
							Intent intent = new Intent(ManageProfile.this, NetworkService.class);
							communicator.setUpdateEmailIntent(intent, _email);
							startService(intent);
						}
						return true;	
					}
					else{
						Common.showToast(ManageProfile.this, "Invalid email.");
						return false;
					}
				}	
				default:
					break;
				}
			}
			return false;
		}
	};
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed(){
		super.onBackPressed();
		Intent returnIntent = new Intent();
		returnIntent.putExtra("isPictureChanged",isPicChanged);
		setResult(Activity.RESULT_OK);
		slideInLeft();
	}
	
	public void slideInLeft(){
		overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_left);
	}

	@Override
	public void onRequestComplete(RequestType requestType, String response) {
		if(response==null){
			Log.d("RequestType: "+requestType.toString(),"Manager Profile: Null response");
			return;
		}
		switch (requestType) {
		case RequestTypeProfileUpdatePassword:
			try{
				JSONObject jobj = new JSONObject(response);
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Common.showToast(ManageProfile.this, "Password updated successfully.");
				}
				else{
					Common.showToast(ManageProfile.this, "Oops! Some error occurred. Please try again.");
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			break;

		default:
			break;
		}
		
	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
		// TODO Auto-generated method stub
		
	}
}
