package cibola.co.in.UI;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;

/**
 * Class for handling collapse and expand animations.
 * @author Esben Gaarsmand
 *
 */
public class ExpandCollapseAnimation extends Animation {
	private View mAnimatedView;
	private int mEndHeight;
	private int mType;

	/**
	 * Initializes expand collapse animation, has two types, collapse (1) and expand (0).
	 * @param view The view to animate
	 * @param duration
	 * @param type The type of animation: 0 will expand from gone and 0 size to visible and layout size defined in xml. 
	 * 1 will collapse view and set to gone
	 */
	public ExpandCollapseAnimation(View view, int duration, int type) {
		setDuration(duration);
		mAnimatedView = view;
		mEndHeight = mAnimatedView.getLayoutParams().height;
		mType = type;
		if(mType == 0) {
			mAnimatedView.getLayoutParams().height = 0;
			mAnimatedView.setVisibility(View.VISIBLE);
		}
	}
	/*
    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        if (interpolatedTime < 1.0f) {
            if(mType == 0) {
                mAnimatedView.getLayoutParams().height = (int) (mEndHeight * interpolatedTime);
            } else {
            	System.out.println("SAGAR PATIDAR");
                mAnimatedView.getLayoutParams().height = mEndHeight - (int) (mEndHeight * interpolatedTime);
            }
            mAnimatedView.requestLayout();
        } else {
            if(mType == 0) {
                mAnimatedView.getLayoutParams().height = mEndHeight;
//            	LayoutParams params = new LayoutParams(
//            	        LayoutParams.MATCH_PARENT,      
//            	        mAnimatedView.getHeight()
//            	);
//            	//params.setMargins(0.0f, top, right, bottom);

                mAnimatedView.requestLayout();
            } else {
            	System.out.println("SAGAR PATIDAR");
                mAnimatedView.getLayoutParams().height = 0;
                mAnimatedView.setVisibility(View.GONE);
                mAnimatedView.requestLayout();
                mAnimatedView.getLayoutParams().height = mEndHeight;
            }
        }
    }*/

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		super.applyTransformation(interpolatedTime, t);
		if (interpolatedTime < 1.0f) {
			if(mType == 0) {
				//mAnimatedView.getLayoutParams().height = (int) (mEndHeight * interpolatedTime);
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT,      
						(int) (mEndHeight * interpolatedTime)
						);
				params.setMargins(0,0,0,0);
				mAnimatedView.setLayoutParams(params);
			} else {
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT,      
						(int) (mEndHeight * interpolatedTime)
						);
				params.setMargins(0,-((int) (mEndHeight * interpolatedTime)),0,0);
				
				mAnimatedView.setLayoutParams(params);
				
				
				//mEndHeight - (int) (mEndHeight * interpolatedTime);
			}
			mAnimatedView.requestLayout();
		} else {
			Log.d("HERE", "HERE");
			if(mType == 0) {
				//mAnimatedView.getLayoutParams().height = mEndHeight;
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT,      
						mEndHeight
						);
				params.setMargins(0,0,0,0);
				
				mAnimatedView.setLayoutParams(params);
				mAnimatedView.requestLayout();
			} else {
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT,      
						mEndHeight
						);
				params.setMargins(0,-mEndHeight,0,0);
				
				mAnimatedView.setLayoutParams(params);
				mAnimatedView.requestLayout();
				//mAnimatedView.getLayoutParams().height = mEndHeight;
			}
		}
	}
}