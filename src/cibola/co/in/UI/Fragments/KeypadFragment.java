
package cibola.co.in.UI.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.CustomKeypadListener;
import cibola.co.in.UI.R;

public class KeypadFragment extends Fragment{

	private CustomKeypadListener keypadListener;
	private String value = "";
	private StringBuilder builder;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		try{
			keypadListener = (CustomKeypadListener) getActivity();
		}
		catch(Exception e){
			Log.d(Constants.DEBUG,"The parent activity uses custom keyboard. Hence, CusomKeypadListener needs to be implemented!");
			e.printStackTrace();
		}
		
		builder = new StringBuilder("");
	}	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.white_keypad,
				container, false);

		TextView key1 = (TextView) rootView.findViewById(R.id.key_1);
		TextView key2 = (TextView) rootView.findViewById(R.id.key_2);
		TextView key3 = (TextView) rootView.findViewById(R.id.key_3);
		TextView key4 = (TextView) rootView.findViewById(R.id.key_4);
		TextView key5 = (TextView) rootView.findViewById(R.id.key_5);
		TextView key6 = (TextView) rootView.findViewById(R.id.key_6);
		TextView key7 = (TextView) rootView.findViewById(R.id.key_7);
		TextView key8 = (TextView) rootView.findViewById(R.id.key_8);
		TextView key9 = (TextView) rootView.findViewById(R.id.key_9);
		TextView key10 = (TextView) rootView.findViewById(R.id.key_10); 
		TextView key0 = (TextView) rootView.findViewById(R.id.key_0);
		
		TextView key2text = (TextView) rootView.findViewById(R.id.key_2_text);
		TextView key3text = (TextView) rootView.findViewById(R.id.key_3_text);
		TextView key4text = (TextView) rootView.findViewById(R.id.key_4_text);
		TextView key5text = (TextView) rootView.findViewById(R.id.key_5_text);
		TextView key6text = (TextView) rootView.findViewById(R.id.key_6_text);
		TextView key7text = (TextView) rootView.findViewById(R.id.key_7_text);
		TextView key8text = (TextView) rootView.findViewById(R.id.key_8_text);
		TextView key9text = (TextView) rootView.findViewById(R.id.key_9_text);
		TextView key0text = (TextView) rootView.findViewById(R.id.key_0_text);
		
		RelativeLayout zero = (RelativeLayout) rootView.findViewById(R.id.group_key_0);
		RelativeLayout one = (RelativeLayout) rootView.findViewById(R.id.group_key_1);
		RelativeLayout two = (RelativeLayout) rootView.findViewById(R.id.group_key_2);
		RelativeLayout three = (RelativeLayout) rootView.findViewById(R.id.group_key_3);
		RelativeLayout four = (RelativeLayout) rootView.findViewById(R.id.group_key_4);
		RelativeLayout five = (RelativeLayout) rootView.findViewById(R.id.group_key_5);
		RelativeLayout six = (RelativeLayout) rootView.findViewById(R.id.group_key_6);
		RelativeLayout seven = (RelativeLayout) rootView.findViewById(R.id.group_key_7);
		RelativeLayout eight = (RelativeLayout) rootView.findViewById(R.id.group_key_8);
		RelativeLayout nine = (RelativeLayout) rootView.findViewById(R.id.group_key_9);
		RelativeLayout clear = (RelativeLayout) rootView.findViewById(R.id.group_key_clear);
		
		
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_LIGHT, getActivity().getAssets(), key1, key2, key3,
				key4, key5, key6, key7, key8, key9, key10, key0);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_LIGHT, getActivity().getAssets(),  key2text,
				key3text, key4text, key5text, key6text, key7text, key8text, key9text, key0text);
		
		zero.setSoundEffectsEnabled(true);
		one.setSoundEffectsEnabled(true);
		two.setSoundEffectsEnabled(true);
		three.setSoundEffectsEnabled(true);
		four.setSoundEffectsEnabled(true);
		five.setSoundEffectsEnabled(true);
		six.setSoundEffectsEnabled(true);
		seven.setSoundEffectsEnabled(true);
		eight.setSoundEffectsEnabled(true);
		nine.setSoundEffectsEnabled(true);
		clear.setSoundEffectsEnabled(true);
		zero.setOnClickListener(onClickListener);
		one.setOnClickListener(onClickListener);
		two.setOnClickListener(onClickListener);
		three.setOnClickListener(onClickListener);
		four.setOnClickListener(onClickListener);
		five.setOnClickListener(onClickListener);
		six.setOnClickListener(onClickListener);
		seven.setOnClickListener(onClickListener);
		eight.setOnClickListener(onClickListener);
		nine.setOnClickListener(onClickListener);
		clear.setOnClickListener(onClickListener);
		return rootView;
	}
	
	
	public void onValueChange(char key, boolean isClear){
		if(isClear){
			int len = builder.length(); 
			if(len>0){
				builder.deleteCharAt(len-1);
				keypadListener.onEnteredValueChange(builder.toString());
			}
		}
		else{
			builder.append(key);
			keypadListener.onEnteredValueChange(builder.toString());
		}
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.group_key_0:
				onValueChange('0', false);
				break;
			case R.id.group_key_1:
				onValueChange('1', false);
				break;
			case R.id.group_key_2:
				onValueChange('2', false);
				break;
			case R.id.group_key_3:
				onValueChange('3', false);
				break;
			case R.id.group_key_4:
				onValueChange('4', false);
				break;
			case R.id.group_key_5:
				onValueChange('5', false);
				break;
			case R.id.group_key_6:
				onValueChange('6', false);
				break;
			case R.id.group_key_7:
				onValueChange('7', false);
				break;
			case R.id.group_key_8:
				onValueChange('8', false);
				break;
			case R.id.group_key_9:
				onValueChange('9', false);
				break;
			case R.id.group_key_clear:
				onValueChange('?', true);
				break;
			default:
				break;
			}
			
		}
	};
	
}
