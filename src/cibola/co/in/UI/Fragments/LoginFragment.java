
package cibola.co.in.UI.Fragments;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.Marketplace;
import cibola.co.in.NetworkManager.NetworkConnectionDetector;
import cibola.co.in.NetworkManager.RequestType;
import cibola.co.in.NetworkManager.Services.NetworkService;
import cibola.co.in.UI.InputMobileActivity;
import cibola.co.in.UI.MainActivity;
import cibola.co.in.UI.R;

public class LoginFragment extends Fragment implements WebConnectionCallbacks{

	
	private EditText username;
	private EditText pwd;
	private TextView loginSubmit;
	private TextView signupSubmit;
	private Marketplace marketplace;
	private MyAccountManager accountManager;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login,
				container, false);
	
		username = (EditText) rootView.findViewById(R.id.login_input_username);
		pwd = (EditText) rootView.findViewById(R.id.login_input_password);
		TextView login_title = (TextView) rootView.findViewById(R.id.login_text02);
		loginSubmit = (TextView) rootView.findViewById(R.id.login_submit);
		signupSubmit = (TextView) rootView.findViewById(R.id.login_sign_up_submit);
		
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), username, pwd, loginSubmit, signupSubmit, login_title);
		pwd.setOnKeyListener(keyListener);
		username.requestFocus();

		marketplace = new Marketplace(getActivity());
		accountManager = new MyAccountManager(getActivity());
		String _username = accountManager.getUsername();
		if(_username!=null){
			username.setText(_username);
			pwd.requestFocus();
		}
		
		loginSubmit.setOnClickListener(onClickListener);
		signupSubmit.setOnClickListener(onClickListener);
		
		if(accountManager.isInitialized()){
			username.setEnabled(false);
			signupSubmit.setVisibility(View.GONE);
		}
		
		if(!accountManager.hasGCMId() && NetworkConnectionDetector.isConnected(getActivity())){
			Intent intent = new Intent(getActivity(), NetworkService.class);
			intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_GET_GCM);
			getActivity().startService(intent);
		}
		else{
			Log.d("GCM ID", accountManager.getGCMId());
		}
		return rootView;
	}
	 
	
	private OnKeyListener keyListener = new OnKeyListener() {
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if ((event.getAction() == KeyEvent.ACTION_UP) &&
					(keyCode == KeyEvent.KEYCODE_ENTER)){
				switch (v.getId()) {
				case R.id.login_input_password:
					Common.hideKeyBoardFromEditText(pwd, getActivity());
					loginSubmit.performClick();
					return true;
				default:
					break;
				}
			}
			return false;
		}
	};

	private OnClickListener onClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.login_submit:
				String passwd = pwd.getText().toString();
				String uname = username.getText().toString();
				//DialogManager dialogManager = new DialogManager();
				//dialogManager.showWebviewDialog(getActivity());
				if(uname.length()>2 && passwd.length()>=0){
					marketplace.signin(getActivity(), uname, passwd);
				}
				else{
					Common.showToast(getActivity(), "Please input valid credentials.");
				}
				break;
			case R.id.login_sign_up_submit:
				startActivity(new Intent(getActivity(), InputMobileActivity.class));
				break;
			default:
				break;
			}

		}
	};

	@Override
	public void onRequestComplete(RequestType requestType, String response) {
		if(response==null){
			Log.d(Constants.BUG, "LoginActivity:onRequestComplete getting null response");
			return;
		}
		try{
			JSONObject jobj = new JSONObject(response);
			switch (requestType) {
			case RequestTypeSignIn:
				int success = jobj.getInt(Constants.WEB_RESPONSE_SUCCESS);
				if(success==1){
					
					if(accountManager.isInitialized()){
						startActivity(new Intent(getActivity(), MainActivity.class));
					}
					else{
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new CibolaInitFragment()).commit();
					}
				}
				else if(success==0){
//					if(accountManager.isInitialized()){
//						startActivity(new Intent(getActivity(), MainActivity.class));
//						
//					}
//					else{
//						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new CibolaInitFragment()).commit();
//					}
					Common.showToast(getActivity(), "Unsuccessful login. Invalid username or password");
				}
				break;
			default:
				break;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

		
	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
		// TODO Auto-generated method stub
		
	}

	
}
