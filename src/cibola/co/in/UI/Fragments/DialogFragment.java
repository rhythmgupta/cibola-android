
package cibola.co.in.UI.Fragments;

import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.CommunicationIntents;
import cibola.co.in.General.Constants;
import cibola.co.in.Interfaces.TransactionCommentDialogCallback;
import cibola.co.in.NetworkManager.Services.NetworkService;
import cibola.co.in.UI.ChatActivity;
import cibola.co.in.UI.R;

public class DialogFragment extends Fragment{

	private EditText comment;
	private TextView done;
	private TextView skip;
	private CommunicationIntents communicator;
	private int amount;
	private String toUsername;
	private boolean isRequested;
	private TransactionCommentDialogCallback callback; 
	private Pattern emptyCommentPattern = Pattern.compile("[\\s]+");
	
	public DialogFragment(Activity activity, String _toUsername, int pAmount, boolean _isRequested){
		amount = pAmount;
		isRequested = _isRequested;
		toUsername = _toUsername;
		communicator = new CommunicationIntents();
		callback = (TransactionCommentDialogCallback) activity;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.dialog_comment, container,
				false);
		comment = (EditText) rootView.findViewById(R.id.dialog_commment);
		done = (TextView) rootView.findViewById(R.id.dialog_comment_done);
		skip = (TextView) rootView.findViewById(R.id.dialog_comment_skip);
		done.setOnClickListener(onClickListener);
		skip.setOnClickListener(onClickListener);
		comment.requestFocus();
	    Common.showSoftInputKeyboard(getActivity(), comment);
	    Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), comment);
	    Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), skip, done);
		return rootView;
	}
	
	private OnClickListener onClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.dialog_comment_done:
			{
				String _comment = comment.getText().toString();
				Intent intent = new Intent(getActivity(), NetworkService.class);
				
				if(_comment==null || _comment.length()==0 || emptyCommentPattern.matcher(_comment).matches()){
					communicator.setTransactionIntent(intent, toUsername, amount, null, isRequested);	
				}
				else{
					_comment = _comment.trim();
					communicator.setTransactionIntent(intent, toUsername, amount, _comment, isRequested);
				}
				
				getActivity().startService(intent);
				dismiss();
				callback.onCommentDialogDismiss();
				break;
			}
			case R.id.dialog_comment_skip:
			{
				Intent intent = new Intent(getActivity(), NetworkService.class);
				communicator.setTransactionIntent(intent, toUsername, amount, null, isRequested);
				getActivity().startService(intent);
				dismiss();
				callback.onCommentDialogDismiss();
				break;
			}
			default:
				break;
			}
			
		}
	};
	
	public void show(FragmentManager fragmentManager) {
		ChatActivity.isCommentDialogOpen = true;
	    FragmentTransaction ft = fragmentManager.beginTransaction();
	    String tag = DialogFragment.class.getName();
	    ft.add(android.R.id.content, this, tag);
	    ft.commit();
	    
	}

	public void dismiss() {
		ChatActivity.isCommentDialogOpen = false;
		Intent intent = new Intent(Constants.INTENT_COMMENT_DIALOG_DISMISSED);
		getActivity().sendBroadcast(intent);
		Common.hideKeyBoardFromEditText(comment, getActivity());
	    FragmentTransaction ft = getFragmentManager().beginTransaction();
	    ft.remove(this);
	    ft.commit();
	    //callback.onCommentDialogDismiss();
	}
	
	
}
