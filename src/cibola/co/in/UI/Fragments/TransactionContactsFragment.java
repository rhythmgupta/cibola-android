
package cibola.co.in.UI.Fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import cibola.co.in.Adapters.PagerAdapter;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.UI.R;

public class TransactionContactsFragment extends Fragment {

	private ImageView button_send_ask;
	private ImageView button_contacts_list ;
	private static ViewPager viewPager;
	private Database db;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);

		viewPager = (ViewPager) rootView.findViewById(R.id.pager);
		if(viewPager==null){
			System.out.println("VIEWPAGER NULL");
		}
//		viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
//
//			@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//			@Override
//			public void transformPage(View page, float position) {
//				final float normalizedposition = Math.abs(Math.abs(position) - 1);
//				page.setScaleX(normalizedposition / 2 + 0.5f);
//				page.setScaleY(normalizedposition / 2 + 0.5f);
//
//			}
//		});

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {

				switch (position) {
				case 0:
					resetImageDrawables(0);
					break;
				case 1:
					resetImageDrawables(1);
					break;
				case 2:
					resetImageDrawables(2);
					break;
				default:
					break;
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});

		viewPager.setAdapter(new PagerAdapter(getActivity().getSupportFragmentManager()));
		viewPager.setBackgroundResource(R.color.white);
		button_send_ask = (ImageView) rootView.findViewById(R.id.icon_send_ask);
		button_contacts_list = (ImageView) rootView.findViewById(R.id.icon_contacts_list);
		
		button_contacts_list.setOnClickListener(onClickListener);
		button_send_ask.setOnClickListener(onClickListener);
		resetImageDrawables(0);
		
		db = new Database(getActivity());
		if(db.isTransactionTableEmpty()){
			button_contacts_list.performClick();
		}
		return rootView;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setPostHoneyAlpha(ImageView imageView, float alpha){
		imageView.setAlpha(alpha);
	}
	
	private void setPreHoneyAlpha(ImageView imageView, int alpha){
		imageView.setAlpha(alpha);
	}
	
	
	
	private void resetImageDrawables(int position){
		switch(position){
		case 0:
			if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB){
				setPostHoneyAlpha(button_contacts_list, 0.44f);
				setPostHoneyAlpha(button_send_ask, 1f);
			}
			else{
				setPreHoneyAlpha(button_contacts_list, 44);
				setPostHoneyAlpha(button_send_ask, 255);
			}
			
			break;
		case 1:
			if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB){
				setPostHoneyAlpha(button_send_ask, 0.44f);
				setPostHoneyAlpha(button_contacts_list, 1f);
			}
			else{
				setPreHoneyAlpha(button_send_ask, 44);
				setPostHoneyAlpha(button_contacts_list, 255);
			}
			break;
		default:
			break;
		}

	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			ImageView image = (ImageView) v;
			switch(v.getId()){
			case R.id.icon_send_ask:
				viewPager.setCurrentItem(0,true);
				break;
			case R.id.icon_contacts_list:
				viewPager.clearDisappearingChildren();
				viewPager.setCurrentItem(1,true);
				break;
			}


		}
	};

}