
package cibola.co.in.UI.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.UI.R;

public class Goldman extends Fragment{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_goldman, container,
				false);
		TextView title = (TextView) rootView.findViewById(R.id.drawer_fragment_title);
		TextView sub_title = (TextView) rootView.findViewById(R.id.drawer_fragment_subtitle);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,getActivity().getAssets(), title, sub_title);
		return rootView;
	}

}
