package cibola.co.in.UI.Fragments;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.NetworkManager.Services.NetworkService;
import cibola.co.in.SqliteManager.DataProvider;
import cibola.co.in.SqliteManager.Database;
import cibola.co.in.SqliteManager.Tables;
import cibola.co.in.UI.ChatActivity;
import cibola.co.in.UI.MainActivity;
import cibola.co.in.UI.OneTimePay;
import cibola.co.in.UI.R;

public class ContactsFragment extends ListFragment implements OnQueryTextListener, OnCloseListener,LoaderCallbacks<Cursor> {


	private ContactsAdapter adapter;
	private MySearchView mSearchView;
	private String mCurFilter;
	private String[] projections = new String[]{
			Tables.contact_serial, Tables.contact_name, Tables.contact_mobile, 
			Tables.contact_background, Tables.contact_is_invited, Tables.contact_onCibola 
	}; 
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		adapter = new ContactsAdapter(getActivity(), null, false);
		
	}	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		getLoaderManager().initLoader(0, null, this);
		ListView lv  = getListView();
		lv.setDividerHeight(1);
		lv.setFastScrollEnabled(true);
		View header = getLayoutInflater(savedInstanceState).inflate(R.layout.row_contacts_header, null);
		TextView text1 = (TextView) header.findViewById(R.id.row_contacts_name);
		TextView text2 = (TextView) header.findViewById(R.id.row_contacts_number);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getActivity().getAssets(), text1,text2);
		header.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(getActivity(),OneTimePay.class));		
			}
		});
		lv.addHeaderView(header);
		setListAdapter(adapter);
	}
	

	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
		Cursor cursor= adapter.getCursor();
		cursor.moveToPosition(position-1);// -1 because there is an additional view - header - quick pay
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(getActivity(), ChatActivity.class);
		intent.putExtra(Constants.CONTACT_NAME, cursor.getString(cursor.getColumnIndex(Tables.contact_name)));
		intent.putExtra(Constants.CONTACT_ID, cursor.getString(cursor.getColumnIndex(Tables.contact_mobile)));
		intent.putExtra(Constants.CONTACT_BACKGROUND, cursor.getInt(cursor.getColumnIndex(Tables.contact_background)));
		startActivity(intent);
	}
	
	public static class MySearchView extends SearchView {
        public MySearchView(Context context) {
            super(context);
        }

        @Override
        public void onActionViewCollapsed() {
            setQuery("", false);
            super.onActionViewCollapsed();
        }
    }


	
	@Override 
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		if(!MainActivity.mNavigationDrawerFragment.isDrawerOpen()){
			MenuItem item =  menu.findItem(R.id.action_search);
	        MenuItemCompat.setShowAsAction( item, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM |
	        		MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
	        item.setIcon(R.drawable.icon_search);
	        mSearchView = new MySearchView(getActivity());
	        mSearchView.setOnQueryTextListener(this);
	        mSearchView.setOnCloseListener(this);
	        mSearchView.setQueryHint("Search Contacts");
	        MenuItemCompat.setActionView(item, mSearchView);
	        customizeSearchView(mSearchView);
		}
    }
	
	private void customizeSearchView(MySearchView mSearchView){
		EditText v = (EditText) mSearchView.findViewById(R.id.search_src_text);
		v.setTextColor(getResources().getColor(R.color.white));
		v.setHintTextColor(getResources().getColor(R.color.white));
	}

    public boolean onQueryTextChange(String newText) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;
        getLoaderManager().restartLoader(0, null, this);
        Log.d("DEBUG","ContactsFragment: loader restarted");
        return true;
    }

    @Override public boolean onQueryTextSubmit(String query) {
        // Don't care about this.
        return true;
    }

    @Override
    public boolean onClose() {
        if (!TextUtils.isEmpty(mSearchView.getQuery())) {
            mSearchView.setQuery(null, true);
        }
        return true;
    }


	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> arg0,
			Cursor arg1) {
		adapter.swapCursor(arg1);
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> arg0) {
		adapter.swapCursor(null);
	}

	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int arg0,
			Bundle arg1) {
		CursorLoader loader ;
		if(mCurFilter==null || mCurFilter.length()==0){
			loader = new CursorLoader(getActivity(), DataProvider.URI_CONTACTS, projections, null, null, 
					Tables.contact_onCibola+" DESC, "+Tables.contact_name+" ASC");
		}
		else{
			String where = Tables.contact_name+" LIKE '"+mCurFilter+"%' OR "+Tables.contact_name+" LIKE '% "+mCurFilter+"%'";
			
			loader = new CursorLoader(getActivity(), DataProvider.URI_CONTACTS, projections, 
					where, null, 
					Tables.contact_onCibola+" DESC, "+Tables.contact_name+" ASC");
			Log.d("Debug","FilterText: "+where);
		}
		return loader;
	}

	private class ContactsAdapter extends CursorAdapter
	{
		private Context mContext;
		private LayoutInflater li;
		private SimpleDateFormat date;
		private Resources resources;
		private PhotoManager photoManager;
		private Database db;
		
		public ContactsAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
			mContext = context;
			li = LayoutInflater.from(context);
			date = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
			date.setTimeZone(TimeZone.getTimeZone("IST"));
			resources = getActivity().getResources();
			photoManager = new PhotoManager();
			db = new Database(context);
		}

		private class ViewHolder_NonCibola{
			private TextView name;
			private TextView number;
			private ImageView invite;
			private ImageView profile_pic;
			private FrameLayout frame;
			public ViewHolder_NonCibola(View v){
				name = (TextView) v.findViewById(R.id.row_contacts_name);
				number = (TextView) v.findViewById(R.id.row_contacts_number);
				invite = (ImageView) v.findViewById(R.id.row_contacts_invite);
				profile_pic = (ImageView) v.findViewById(R.id.profile_pic);
				frame = (FrameLayout) v.findViewById(R.id.row_contact_frame);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), name, number);
			}
		}
		
		private class ViewHolder_OnCibola{
			private TextView name;
			private TextView number;
			private ImageView profile_pic;
			private FrameLayout frame;
			public ViewHolder_OnCibola(View v){
				name = (TextView) v.findViewById(R.id.row_contacts_name);
				number = (TextView) v.findViewById(R.id.row_contacts_number);
				profile_pic = (ImageView) v.findViewById(R.id.profile_pic);
				frame = (FrameLayout) v.findViewById(R.id.row_contact_frame);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), name, number);
			}
		}
		
		
		@Override
		public int getItemViewType(int position) {
			Cursor cursor = (Cursor) getItem(position);
			if(cursor==null){
				Log.d("Debug-Cibola","Null Cursor | contactsFragment.java - getItemViewType position="+position);
			}
			int onCibola = cursor.getInt(cursor.getColumnIndex(Tables.contact_onCibola));
			if(onCibola == 0){
				return 0;
			}
			else{
				return 1;
			}
		}

		@Override
		public int getViewTypeCount() {
			return 2;
		}

		
		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			if(view.getTag() instanceof ViewHolder_NonCibola){
				ViewHolder_NonCibola holder = (ViewHolder_NonCibola) view.getTag();
				String name = cursor.getString(cursor.getColumnIndex(Tables.contact_name));
				
				holder.name.setText(Html.fromHtml(Common.getSearchHighlightedString(mCurFilter, name)), 
										TextView.BufferType.SPANNABLE);
				final String _number = cursor.getString(cursor.getColumnIndex(Tables.contact_mobile)); 
				holder.number.setText(_number);
				holder.profile_pic.setImageBitmap(photoManager.getCibolaUserPic(mContext, _number, name, ImageType.Medium));
				holder.profile_pic.setBackgroundColor(cursor.getInt(cursor.getColumnIndex(Tables.contact_background)));
				
				if(cursor.getInt(cursor.getColumnIndex(Tables.contact_is_invited))==1){
					holder.invite.setImageResource(R.drawable.button_row_contacts_invited);
				}
				else{
					holder.invite.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(getActivity(),NetworkService.class);
							intent.putExtra(Constants.NETWORK_SERVICE_CODE, Constants.NETWORK_SERVICE_CODE_INVITE);
							intent.putExtra(Constants.INVITE_CONTACT_NUMBER, _number);
							getActivity().startService(intent);
							db.updateInviteStatus(_number, 1);
						}
					});
				}
			}
			else if(view.getTag() instanceof ViewHolder_OnCibola){
				ViewHolder_OnCibola holder = (ViewHolder_OnCibola) view.getTag();
				String name = cursor.getString(cursor.getColumnIndex(Tables.contact_name));
				final String _number = cursor.getString(cursor.getColumnIndex(Tables.contact_mobile));
				holder.name.setText(Html.fromHtml(Common.getSearchHighlightedString(mCurFilter, name)), 
										TextView.BufferType.SPANNABLE);
				holder.number.setText(_number);
				holder.profile_pic.setImageBitmap(photoManager.getCibolaUserPic(mContext, _number, name, ImageType.Medium));
				holder.profile_pic.setBackgroundColor(cursor.getInt(cursor.getColumnIndex(Tables.contact_background)));
				
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			if(cursor.getInt(cursor.getColumnIndex(Tables.contact_onCibola))==0){
				ViewHolder_NonCibola holder;
				View view;
				view = li.inflate(R.layout.row_contacts_non_cibola, parent, false);
				holder = new ViewHolder_NonCibola(view);
				holder.frame.setForeground(resources.getDrawable(R.drawable.contact_wireframe_cibola_mobile));
				view.setTag(holder);
				return view;
			}
			else{
				ViewHolder_OnCibola holder;
				View view;
				view = li.inflate(R.layout.row_contacts_cibola, parent, false);
				
				holder = new ViewHolder_OnCibola(view);
				holder.frame.setForeground(resources.getDrawable(R.drawable.contact_wireframe_cibola));
				view.setTag(holder);
				return view;
			}
		}
	}

}
