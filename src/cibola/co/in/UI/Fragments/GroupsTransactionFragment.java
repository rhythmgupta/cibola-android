package cibola.co.in.UI.Fragments;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.SqliteManager.DataProvider;
import cibola.co.in.SqliteManager.Tables;
import cibola.co.in.UI.R;

public class GroupsTransactionFragment extends ListFragment implements LoaderCallbacks<Cursor> {


	private ContactsAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		adapter = new ContactsAdapter(getActivity(), null, false);
		setListAdapter(adapter);
	}	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(0, null, this);
		ListView lv  = getListView();
		//lv.setDivider(new ColorDrawable(this.getResources().getColor(R.color.common_line)));
		lv.setDividerHeight(1);
		lv.setVerticalScrollBarEnabled(false);
		lv.setHorizontalScrollBarEnabled(false);
		
	}    
	
	
	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> arg0,
			Cursor arg1) {
		adapter.swapCursor(arg1);
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> arg0) {
		adapter.swapCursor(null);
	}

	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int arg0,
			Bundle arg1) {
		CursorLoader loader = new CursorLoader(getActivity(), DataProvider.URI_CONTACTS, null, null, null, null);
		
		return loader;
	}
	
	private class ContactsAdapter extends CursorAdapter
	{
		private Context mContext;
		private LayoutInflater li;
		private SimpleDateFormat date;
		public ContactsAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
			mContext = context;
			li = LayoutInflater.from(context);
			date = new SimpleDateFormat("MMM dd, yyyy");
			date.setTimeZone(TimeZone.getTimeZone("IST"));
		}
		
		private class ViewHolder{
			private TextView name;
			private ImageView profile_pic;
			public ViewHolder(View v){
				name = (TextView) v.findViewById(R.id.row_contacts_name);
				profile_pic = (ImageView) v.findViewById(R.id.profile_pic);
				Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), name);
			}
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			// TODO Auto-generated method stub
			ViewHolder holder = (ViewHolder) view.getTag();
			holder.name.setText(cursor.getString(cursor.getColumnIndex(Tables.contact_name)));
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			ViewHolder holder;
			View view;
			view = li.inflate(R.layout.row_contacts_non_cibola, parent, false);
			holder = new ViewHolder(view);
			view.setTag(holder);
			return view;
		}

		
	}

}
