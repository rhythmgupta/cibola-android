package cibola.co.in.UI;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.NetworkConnectionDetector;
import cibola.co.in.NetworkManager.RequestType;
import cibola.co.in.NetworkManager.Services.ContactsService;
import cibola.co.in.UI.Fragments.CibolaInitFragment;
import cibola.co.in.UI.Fragments.LoginFragment;

public class LoginActivity extends ActionBarActivity implements WebConnectionCallbacks {

//	private MyReceiver receiver;
	private MyAccountManager accountManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		getSupportActionBar().hide();
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, new LoginFragment()).commit();
		}
		
//		receiver = new MyReceiver();
//		IntentFilter filter = new IntentFilter(Constants.CONTACTS_UPDATE_COMPLETE_BROADCAST_ACTION);
//		registerReceiver(receiver, filter);
		accountManager = new MyAccountManager(getApplicationContext());
		if(NetworkConnectionDetector.isConnected(this) && !accountManager.isInitialized()){
			Intent intent = new Intent(this,ContactsService.class);
			intent.putExtra(Constants.CONTACTS_MANAGER_IS_INIT, 1);
			startService(intent);	
		}
		else{
			Common.showToast(this, "Please connect to a working internet connection");
		}
		
	}

	
	@Override
	public void onRequestComplete(RequestType requestType, String response) {
		if(response==null){
			Log.d(Constants.BUG,"Oops! Some error occurred!");
			return;
		}
		try{
			JSONObject jobj = new JSONObject(response);
			switch (requestType) {
			
			case RequestTypeSignIn:
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Intent i = new Intent(LoginActivity.this,MainActivity.class);
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
//					LoginFragment loginFragment = (LoginFragment)getSupportFragmentManager().findFragmentById(R.id.container);
//					loginFragment.onRequestComplete(requestType, response);
				}
				else{
					Common.showToast(this, "Some error occurred. Please try again.");
				}
				break;
			case RequestTypeInitContacts:
				CibolaInitFragment cibolaInitFragment = (CibolaInitFragment) getSupportFragmentManager().findFragmentById(R.id.container);
				cibolaInitFragment.onRequestComplete(requestType, response);
			default:
				break;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
		
	}
	
	@Override
	public void onPause(){
		super.onPause();
//		unregisterReceiver(receiver);
	}
	
	@Override
	public void onResume(){	
		super.onResume();
//		IntentFilter filter = new IntentFilter(Constants.CONTACTS_UPDATE_COMPLETE_BROADCAST_ACTION);
//		registerReceiver(receiver, filter);
	}

	
//	private class MyReceiver extends BroadcastReceiver{
//
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			int verificationMsg = intent.getIntExtra(Constants.CONTACTS_UPDATE_COMPLETE_BROADCAST_MESSAGE,-1);
//			if(verificationMsg==1)
//			{
//				Intent i = new Intent(LoginActivity.this,MainActivity.class);
//				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				startActivity(i);
//				finish();
//			}
//			else
//				Log.d("#DEBUG","RECEIVED NULL MSG");
//
//		}
//	}
}
