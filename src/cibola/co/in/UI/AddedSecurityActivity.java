package cibola.co.in.UI;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;

public class AddedSecurityActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_added_security);
		
		EditText newPwd = (EditText) findViewById(R.id.added_security_new_pwd);
		EditText cnfPwd = (EditText) findViewById(R.id.added_security_cnf_pwd);
		
		TextView text01 = (TextView) findViewById(R.id.added_security_text1);
		TextView text02 = (TextView) findViewById(R.id.added_security_text2);
		TextView savePassword = (TextView) findViewById(R.id.added_security_change_pwd);
		TextView setQuestion = (TextView) findViewById(R.id.added_security_set_ques);
		TextView question = (TextView) findViewById(R.id.added_security_question);
		TextView changeQuestion = (TextView) findViewById(R.id.added_security_change_question);
		
		//savePassword.setBackgroundColor(getResources().getColor(R.color.added_security_button_bg));
		Common.setEditTextFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), newPwd, cnfPwd);
		Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR, getAssets(), text01, text02, savePassword, setQuestion, 
				question, changeQuestion);
		
		Common.setupActionBar(getSupportActionBar());
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			super.onBackPressed();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	

}
