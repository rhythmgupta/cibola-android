/**
 *	SaveImageToDisk.java
 *  Created by Rhythm Gupta on Sep 7, 2014, 3:17:52 PM
 *	Copyright � 2014 by Cibola Technologies Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Graphics;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.NetworkManager.ImageType;
import cibola.co.in.UI.R;

public class PhotoManager {
	
	public void saveImageToDisk(Bitmap imageData, String filename, CompressFormat format, ImageType type){
		File directory = null;
		if(type==ImageType.Medium){
			directory = new File(Environment.getExternalStorageDirectory()
	            + Constants.CIBOLA_MEDIUM_IMAGE_DIR);
		}
		else if(type==ImageType.Large){
			directory = new File(Environment.getExternalStorageDirectory()
		            + Constants.CIBOLA_LARGE_IMAGE_DIR);
		}
		else if(type==ImageType.Self){
			directory = new File(Environment.getExternalStorageDirectory()
		            + Constants.CIBOLA_MAIN_IMAGE_DIR);
		}
		
	    if (!directory.exists()) {
	        directory.mkdirs();
	    }
	    
	    try{
	    	String filePath = directory.toString()+"/" + filename;
	    	if(type==ImageType.Self){
	    		filePath = Constants.CIBOLA_USER_PROFILE_IMAGE_FILEPATH;
	    	}
	    	Log.d("FILEPATH",filePath);
	    	FileOutputStream fos = new FileOutputStream(filePath);
	    	BufferedOutputStream bos = new BufferedOutputStream(fos);
	    	imageData.compress(format, 100, bos);
	    	bos.flush();
	    	bos.close();
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	}
	
	
	public Bitmap getSelfImage(){
		String filePath =  Common.getContactImagePath(null, ImageType.Self);
		File file = new File(filePath);
		if(file.exists()){
			return Bitmap.createBitmap(BitmapFactory.decodeFile(filePath));
		}
		else{
			Log.d(Constants.DEBUG,"Profile image not found");
			return null;
		}
	} 
	
	/**
	 *particularly for chat fragment - instead of alphabets a default user pic will be shown 
	 */
	public Bitmap getSelfChatPic(Context context){
		String filePath = Common.getContactImagePath(null, ImageType.Self);
		File file = new File(filePath);
		if(file.exists()){
			return Bitmap.createBitmap(BitmapFactory.decodeFile(filePath));
		}
		else{
			return BitmapFactory.decodeResource(context.getResources(), R.drawable.default_user);
		}
	}
	
	/**
	 * particularly for chat fragment - instead of alphabets a default user pic will be shown
	 */
	public Bitmap getCibolaUserChatPic(Context context, String contactNumber, ImageType type){
		String filePath = Common.getContactImagePath(contactNumber, type);
		
		//Log.d(Constants.DEBUG,filePath);
		File file = new File(filePath);
		if(file.exists()){
			return Bitmap.createBitmap(BitmapFactory.decodeFile(filePath));
		}
		else{
			//Log.d(Constants.DEBUG,"User pic not found. "+contactNumber);
			return BitmapFactory.decodeResource(context.getResources(), R.drawable.default_user);
			 
		}
	}

	
	public Bitmap getCibolaUserPic(Context context, String contactNumber, String contactName, ImageType type){
		String filePath = Common.getContactImagePath(contactNumber, type);
		
		//Log.d(Constants.DEBUG,filePath);
		File file = new File(filePath);
		if(file.exists()){
			return Bitmap.createBitmap(BitmapFactory.decodeFile(filePath));
		}
		else{
			//Log.d(Constants.DEBUG,"User pic not found. "+contactNumber);
			return BitmapFactory.decodeResource(context.getResources(), Common.getDrawable(contactName));
			 
		}
	}
	
	
}
