/**
 *	EditAccountCallbacks.java
 *  Created by Rhythm Gupta on 14-Aug-2014, 4:29:20 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Interfaces;

public interface EditAccountCallbacks {
	
	void onAccountEditClicked(String number, String name, String ifsc);
	
}