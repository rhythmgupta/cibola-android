
package cibola.co.in.Interfaces;


public interface RefundAccountListCallBack {

	public void onClickAddAccount();
	
	public void extractAccountDetails(String accName, String accNumber, String accIFSC); 
	
}
