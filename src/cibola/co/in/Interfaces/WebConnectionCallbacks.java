
package cibola.co.in.Interfaces;

import cibola.co.in.NetworkManager.RequestType;

public interface WebConnectionCallbacks {

	public void onRequestComplete(RequestType requestType, String response);

	public void onRequestError(RequestType requestType, String errorResponse);
}
