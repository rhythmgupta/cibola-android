/**
 *	DialogManager.java
 *  Created by Rhythm Gupta on 08-Aug-2014, 7:54:02 PM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.General;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import cibola.co.in.UI.R;

public class DialogManager {


	public static void showDateDialog(Context context, final EditText d){
		final Dialog dialog = new Dialog(context);
		dialog.setTitle("Date of Birth");

		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.setContentView(R.layout.dialog_date_picker);
		Button ok = (Button) dialog.findViewById(R.id.select_date);
		
		final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
		String prevDate = d.getText().toString();
		if(prevDate.length()>2){ // 2 is just some arbit number.
			String[] prev = prevDate.split("/");
			datePicker.updateDate(Integer.parseInt(prev[2]), Integer.parseInt(prev[1]), Integer.parseInt(prev[0]));
			
		}
		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Log.d("DEBUG#6",datePicker.toString());
				String dob = datePicker.getDayOfMonth()+"/"+(datePicker.getMonth()+1)+"/"+datePicker.getYear();
				d.setText(dob);
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message){
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		alertDialog.show();
	}
	
	
	public void showWebviewDialog(Context context){
		AlertDialog.Builder alert = new AlertDialog.Builder(context); 
		WebView wv = new WebView(context);
		wv.loadUrl("http://cibola.co.in/webview/");
		wv.setWebViewClient(new WebViewClient() {
		    @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) {
		        view.loadUrl(url);

		        return true;
		    }
		});
		alert.setView(wv);
		alert.show();
	}

	public static void showInternetError(Context context){
		View v = Common.getCustomNoteView(context, ((Activity) context).getLayoutInflater(), 
				context.getResources().getString(R.string.custom_note_no_internet_connection_title),
				context.getResources().getString(R.string.custom_note_no_internet_connection_description), 
				R.drawable.icon_custom_note_no_internet,
				context.getResources().getColor(R.color.toast_internet_bg), null);
		ImageView close = (ImageView) v.findViewById(R.id.custom_note_close);
		close.setImageDrawable(null);
		final Toast toast = new Toast(context);
		//toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setView(v);
		close.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				toast.cancel();
				
			}
		});
		 new CountDownTimer(5000, 1000)
         {
             public void onTick(long millisUntilFinished) {toast.show();}
             public void onFinish() {toast.cancel();}
         }.start();
		
	}
}
