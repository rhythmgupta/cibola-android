/**
 *	NavigationDrawerListAdapter.java
 *  Created by Rhythm Gupta on 04-Aug-2014, 1:35:16 AM
 *	Copyright � 2014 by Cibola Pvt. Ltd
 *	All rights reserved.
 */

package cibola.co.in.Adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.UI.R;

public class NavigationDrawerListAdapter extends ArrayAdapter<NavigationElement>{

	private ArrayList<NavigationElement> data;
	private Context mContext;
	
	public NavigationDrawerListAdapter(Context context, int resource,
			List<NavigationElement> objects) {
		super(context, resource, objects);
		data = (ArrayList<NavigationElement>) objects;
		mContext = context;
	}
	
	public View getView(int position, View convertView, ViewGroup parent){

		View v = convertView;

		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.row_drawer, null);
		}

		
		NavigationElement rowElement = data.get(position);

		if (rowElement != null) {


			TextView first_text = (TextView) v.findViewById(R.id.drawer_list_row_first);
			TextView second_text = (TextView) v.findViewById(R.id.drawer_list_row_second);
			ImageView drawer_icon = (ImageView) v.findViewById(R.id.drawer_list_row_icon);
			Common.setTextViewFontStyle(Constants.FONT_ROBOTO_REGULAR,mContext.getAssets(), first_text,second_text);
			first_text.setText(rowElement.getFirstText());
			second_text.setText(rowElement.getSecondText());
			drawer_icon.setImageDrawable(mContext.getResources().getDrawable(rowElement.getIconID()));
		}

		return v;

	}

	
	
	
	
}
