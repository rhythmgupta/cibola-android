
package cibola.co.in.NetworkManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.DialogManager;

public class ServerConnector 
{
	private Request request;
	private HttpURLConnection httpConnection;
	private ProgressDialog pDialog;
	private Context mContext;

	public ServerConnector(Context pContext,Request pRequest) {
		setRequest(pRequest); 
		mContext = pContext;
	}

	//use this constructor to show processing dialog while it fetches responses from the server
	public ServerConnector(Request pRequest, Context pContext,  String dialogMessage, boolean pCancelable){
		setRequest(pRequest);
		pDialog = Common.getProgressDialog(pContext, dialogMessage, pCancelable);
		mContext = pContext;
	}

	public Request getRequest() 
	{
		return request;
	}

	public void setRequest(Request request) 
	{
		this.request = request;
	}

	public String start() 
	{
		if(NetworkConnectionDetector.isConnected(mContext)){
			if(request==null){
				Log.d(Constants.BUG,"Request is not set!!");
				return null;
			}
			if(request.isToDoInAsyncTask()){
				new DataPost().execute();
				return null;
			}
			else{
				return processRequest();
			}
		}
		else{
//			Handler handler = new Handler() {
//				@Override
//				public void handleMessage(Message msg) {

					//Call you method in This one...
					if(mContext instanceof Activity)
						DialogManager.showInternetError(mContext);
//				}
//			};
//			handler.sendEmptyMessage(0);
			return null;
		}
	}

	private String processRequest(){
		String output="null";
		try 
		{
			Log.d(Constants.DEBUG,"Executing URL is: "+request.getURL());
			URL url = new URL(request.getURL());
			httpConnection = (HttpURLConnection) url.openConnection();

			//set request type
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			httpConnection.setRequestProperty("charset", "EN-US");

			//set request headers
			Map<String,String> reqHeaders;
			if((reqHeaders = request.getHeader())!=null) 
			{
				for(Map.Entry<String, String> row : reqHeaders.entrySet()) 
				{
					httpConnection.setRequestProperty(row.getKey(), row.getValue());
				}
			}
			reqHeaders = null;

			//sending post request if there is atleast one parameter
			Map<String,String> parameters;
			if((parameters = request.getParameters())!=null) 
			{
				String URLParameters = "";
				int totalParams = parameters.size();
				int i =0;
				for(Map.Entry<String, String> row : parameters.entrySet()) 
				{
					if(i<totalParams-1) 
					{
						URLParameters += (row.getKey()+"="+row.getValue()+"&");
					}
					else 
					{
						URLParameters += (row.getKey()+"="+row.getValue());
					}
					i++;
				}
				//Log.d("URL Parameters", URLParameters);
				httpConnection.setDoOutput(true);
				httpConnection.setRequestProperty("Content-Length", Integer.toString(URLParameters.getBytes().length));
				DataOutputStream ostream = new DataOutputStream(httpConnection.getOutputStream());
				Log.d("URLParameters",URLParameters);
				ostream.writeBytes(URLParameters);
				ostream.flush();
				ostream.close();
			}
			parameters = null;

			int responseCode = httpConnection.getResponseCode();
			Log.d(request.getURL()+" :Response Code", responseCode+"");
			if(responseCode==200){
				//collecting response
				BufferedReader in = new BufferedReader(
						new InputStreamReader(httpConnection.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while((inputLine = in.readLine())!=null) 
				{
					response.append(inputLine);
				}

				in.close();
				output = response.toString();
				if(output==null){
					Log.d("ServerConnector:Response", output);
				}
				//				Log.d("ServerConnector:Response", output);
			}
			else{

				Log.d("Error",httpConnection.getErrorStream().toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}

	private class DataPost extends AsyncTask<Void, Void, String>
	{
		@Override
		protected void onPreExecute() 
		{
			if(pDialog!=null){
				pDialog.show();
			}
		}

		@Override
		protected String doInBackground(Void... params) 
		{
			return processRequest();
		}

		@Override
		protected void onPostExecute(String result) 
		{
			if(pDialog!=null){
				pDialog.dismiss();
			}
			getRequest().onComplete(result);
		}

	}
}
