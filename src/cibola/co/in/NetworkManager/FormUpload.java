
package cibola.co.in.NetworkManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class FormUpload
{
	private Request request;
	private HttpURLConnection httpConnection;
	private DataOutputStream ostream ;
	
	private String lineEnd = "\r\n";
	private String twoHyphens = "--";
	private String boundary = "*****";
	private ProgressDialog pDialog;
	private Context mContext;
	public FormUpload(Request pRequest) 
	{
		setRequest(pRequest); 
	}
	
	public FormUpload(Request pRequest, Context pContext,  String dialogMessage, boolean pCancelable){
		setRequest(pRequest);
		pDialog = Common.getProgressDialog(pContext, dialogMessage, pCancelable);
		mContext = pContext;
	}

	public Request getRequest() 
	{
		return request;
	}

	public void setRequest(Request request) 
	{
		this.request = request;
	}

	public void start() 
	{
		
		if(NetworkConnectionDetector.isConnected(mContext)){
			if(request.isToDoInAsyncTask()){
				new DataPost().execute();
			}
			else{
				processRequest();
			}
		}
			
			
		else
			Log.d("DEBUG","FormUpload: Not connected to the internet.");
	}
	
	private String processRequest(){
		String output="null";
		try 
		{
			URL url = new URL(request.getURL());
			httpConnection = (HttpURLConnection) url.openConnection();
			
		    String fileName = "image.png";
	        
			//set request type
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Connection", "Keep-Alive");
			httpConnection.setRequestProperty("ENCTYPE", "multipart/form-data");
			httpConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			httpConnection.setRequestProperty(Constants.WEB_UPLOAD_IMAGE, fileName);
			httpConnection.setDoInput(true);
	        httpConnection.setDoOutput(true);
	        httpConnection.setUseCaches(false);
	        httpConnection.setConnectTimeout(30000);
	        httpConnection.setReadTimeout(30000);
	        
			//set request headers
			Map<String,String> reqHeaders;
			if((reqHeaders = request.getHeader())!=null) 
			{
				for(Map.Entry<String, String> row : reqHeaders.entrySet()) 
				{
					httpConnection.setRequestProperty(row.getKey(), row.getValue());
				}
			}
			reqHeaders = null;

			File file = new File(request.getFileURI());
			if(!file.exists()) {
				Log.d("#DEBUG","File Doesn't Exist: "+request.getFileURI());
				return null;
			}
			FileInputStream fileInputStream = new FileInputStream(file);
	        byte[] bytes = new byte[(int) file.length()];
	        fileInputStream.read(bytes);
	        fileInputStream.close();
	        
			ostream = new DataOutputStream(httpConnection.getOutputStream());
			ostream.writeBytes(twoHyphens + boundary + lineEnd);
			ostream.writeBytes("Content-Disposition: form-data; name=\""+Constants.WEB_UPLOAD_IMAGE+"\";filename=\"" + fileName + "\"" + lineEnd);
	        ostream.writeBytes("Content-Type: image/png" + lineEnd);
	        //ostream.writeBytes("Content-Length: " + bytes.length*2 + lineEnd);
	        ostream.writeBytes(lineEnd);
			
	        int bufferLength = 1024;
	        for (int i = 0; i < bytes.length; i += bufferLength) {
//	            publishing the progress....
//	            Bundle resultData = new Bundle();
//	            resultData.putInt("progress" ,(int)((i / (float) bytes.length) * 100));
//	            receiver.send(UPDATE_PROGRESS, resultData);
	            if (bytes.length - i >= bufferLength) {
	                ostream.write(bytes, i, bufferLength);
	            } else {
	                ostream.write(bytes, i, bytes.length - i);
	            }
	        }
	        
	        //end output
	        ostream.writeBytes(lineEnd);
	        ostream.writeBytes(twoHyphens + boundary + lineEnd);
			//sending post request if there is atleast one parameter
			Map<String,String> parameters;
			if((parameters = request.getParameters())!=null) 
			{
				for(Map.Entry<String, String> row : parameters.entrySet()) 
				{
					writeParam(row.getKey(), row.getValue());
				}
				//Log.d("URL Parameters", URLParameters);
				//httpConnection.setRequestProperty("Content-Length", Integer.toString(URLParameters.getBytes().length));
			}
			parameters = null;
			ostream.flush();
			ostream.close();
			
			// Collecting response
			int responseCode = httpConnection.getResponseCode();
			Log.d("Response Code", responseCode+"");
			if(responseCode==200){
				//collecting response
				BufferedReader in = new BufferedReader(
						new InputStreamReader(httpConnection.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while((inputLine = in.readLine())!=null) 
				{
					response.append(inputLine);
				}

				in.close();
				output = response.toString();
				Log.d("Response", output);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;

	}

	private void writeParam(String key, String value){
		try{
		ostream.writeBytes(twoHyphens + boundary + lineEnd);
		ostream.writeBytes("Content-Disposition: form-data; name=\""+key+"\"" + lineEnd);
		ostream.writeBytes(lineEnd);
		ostream.writeBytes(value + lineEnd);
		ostream.writeBytes(twoHyphens + boundary + lineEnd);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private class DataPost extends AsyncTask<Void, Void, String>
	{
		
		@Override
		protected void onPreExecute() 
		{
			if(pDialog!=null)
			{
				pDialog.show();
			}
		}

		@Override
		protected String doInBackground(Void... params) {
			return processRequest();
		}

		@Override
		protected void onPostExecute(String result) 
		{
			if(pDialog!=null)
			{
				pDialog.dismiss();
			}
			
			if(result!=null){
				getRequest().onComplete(result);
			}
			else{
				Log.d("Bug", "FormUpload:onPostExecute- Getting null result");
			}
		}
		
		
	}
}
