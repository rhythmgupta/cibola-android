
package cibola.co.in.NetworkManager;

public enum RequestType {
	
	RequestTypeTesting,
	
	RequestTypeRegisterGcmOnGoogle,
	RequestTypeUnregisterGcmOnGoogle,
	RequestTypeRegisterGcmOnCibola,
	RequestTypeUnregisterGcmOnCibola,	
	
	RequestTypeSignUpStepOne,
	RequestTypeSignUpStepTwo,
	RequestTypeSignUpStepThree,
	RequestTypeSignIn,
	
	RequestTypeInitContacts,
	RequestTypeUpdateContactsList,
	RequestTypeInviteContact,
	
	RequestTypeNewTransaction,
	RequestTypeUpdateComment,
	RequestTypeUpdateStatus,
	
	RequestTypeProfileUpdateBalance,
	RequestTypeProfileUpdatePic,
	RequestTypeProfileUpdateName,
	RequestTypeProfileUpdateEmail,
	RequestTypeProfileUpdatePassword,
	RequestTypeProfileUpdateSecurityQues,
	
}
