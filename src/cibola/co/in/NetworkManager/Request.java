package cibola.co.in.NetworkManager;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import cibola.co.in.Interfaces.WebConnectionCallbacks;

public class Request 
{
	private String URL;
	private Map<String,String> param;
	private Map<String, String> header;
	private RequestType requestType;
	private WebConnectionCallbacks callbacks;
	private boolean wannaUploadImage = false;
	private String fileURI = null;
	private boolean doInAsyncTask = true;
	public Request(Activity activity,String pURL, RequestType type, boolean enableCallbacks) 
	{
		this.URL = pURL;
		this.requestType = type;
		if(enableCallbacks)
			this.callbacks = (WebConnectionCallbacks) activity;
	}

	public Request(Activity activity, String pURL, RequestType type, String uri, boolean enableCallbacks) 
	{
		this.URL = pURL;
		this.requestType = type;	
		this.wannaUploadImage = true;
		this.setFileURI(uri);
		if(enableCallbacks)
			this.callbacks = (WebConnectionCallbacks) activity;
	}
	

	// Getter and setter for URL
	public String getURL()
	{
		return URL;
	}
	public void setURL(String pURL)
	{
		this.URL = pURL;
	}

	// Getter and Adder for Header
	public Map<String, String> getHeader()
	{
		return header;
	}
	public void addHeader(String pKey, String pValue)
	{
		if(this.header==null)
		{
			this.header = new HashMap<String,String>();
		}
		this.header.put(pKey, pValue);
	}

	// Getter and Adder for Request Parameter
	public Map<String, String> getParameters()
	{
		return this.param;
	}
	public void addParam(String pKey, String pValue)
	{
		if(this.param==null)
		{
			this.param = new HashMap<String,String>();
		}
		this.param.put(pKey,pValue);
	}
	public RequestType getRequestType() {
		return requestType;
	}
	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	public boolean isWannaUploadImage() {
		return wannaUploadImage;
	}

	public void setWannaUploadImage(boolean wannaUploadImage) {
		this.wannaUploadImage = wannaUploadImage;
	}	

	public void onComplete(String response){
		if(callbacks!=null)
			callbacks.onRequestComplete(requestType, response);
	}

	public void onError(String errorResponse){
		if(callbacks!=null)
			callbacks.onRequestError(requestType, errorResponse);
	}

	public String getFileURI() {
		return fileURI;
	}

	public void setFileURI(String fileURI) {
		this.fileURI = fileURI;
	}

	public boolean isToDoInAsyncTask() {
		return doInAsyncTask;
	}

	public void setDoInAsyncTask(boolean doInAsyncTask) {
		this.doInAsyncTask = doInAsyncTask;
	}


}
