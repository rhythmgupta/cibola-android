
package cibola.co.in.NetworkManager.Services;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.ImageDownloadObject;
import cibola.co.in.NetworkManager.ImageDownloader;
import cibola.co.in.NetworkManager.Marketplace;
import cibola.co.in.NetworkManager.RequestType;
import cibola.co.in.SqliteManager.Database;

public class NetworkService extends IntentService implements WebConnectionCallbacks{

	public static boolean isRunning = false;
	private Database database;
	private MyAccountManager accountManager;
	public NetworkService(String name) {
		super(name);
	}
	
	public NetworkService(){
		super("ContactsService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		database = new Database(getApplicationContext());
		accountManager = new MyAccountManager(NetworkService.this);
		if(intent==null){
			Log.d(Constants.BUG, "Intent is null!! How? Who triggered this service?");
		}
		else{
			int networkServiceCode = intent.getIntExtra(Constants.NETWORK_SERVICE_CODE, -1);
			Marketplace marketplace = new Marketplace(null);
			switch (networkServiceCode) {
			case Constants.NETWORK_SERVICE_CODE_TRANSACTION:
			{
				int amount = intent.getIntExtra(Constants.WEB_TRANSACTION_AMOUNT, 0);
				String comment = intent.getStringExtra(Constants.WEB_TRANSACTION_COMMENT);
				String to = intent.getStringExtra(Constants.WEB_TRANSACTION_TO_USERNAME);
				boolean isRequested = intent.getBooleanExtra(Constants.TRANSACTION_IS_REQUESTED, true);
				onReceiveResponse(Constants.NETWORK_SERVICE_CODE_TRANSACTION, 
						marketplace.execNewTransaction(getApplicationContext(), Common.generateTransactionId(), 
						to, comment, amount, isRequested));
				Log.d("DEBUG "+networkServiceCode,"OnReceiveResponse");
				break;
			}
			case Constants.NETWORK_SERVICE_CODE_GET_GCM:
			{
				getGCMRegID(NetworkService.this);
				break;
			}
			case Constants.NETWORK_SERVICE_CODE_REGISTER_GCM_ON_CIBOLA:
			{	
				onReceiveResponse(Constants.NETWORK_SERVICE_CODE_REGISTER_GCM_ON_CIBOLA, 
						marketplace.registerGCM(NetworkService.this, accountManager.getGCMId()));
				break;
			}
			case Constants.NETWORK_SERVICE_CODE_INVITE:
			{
				String _mobile = intent.getStringExtra(Constants.INVITE_CONTACT_NUMBER);
				onReceiveResponse(Constants.NETWORK_SERVICE_CODE_INVITE, marketplace.invite(getApplicationContext(), _mobile));
				break;
			}
			case Constants.NETWORK_SERVICE_CODE_UPDATE_NAME:
			{
				String _fname = intent.getStringExtra(Constants.WEB_UPDATE_FIRST_NAME);
				String _lname = intent.getStringExtra(Constants.WEB_UPDATE_LAST_NAME);
				onReceiveResponse(Constants.NETWORK_SERVICE_CODE_UPDATE_NAME, marketplace.updateName(getApplicationContext(), _fname, _lname));
				break;
			}
			case Constants.NETWORK_SERVICE_CODE_UPDATE_EMAIL:
			{
				String _email = intent.getStringExtra(Constants.WEB_UPDATE_EMAIL);
				onReceiveResponse(Constants.NETWORK_SERVICE_CODE_UPDATE_EMAIL, marketplace.updateEmail(getApplicationContext(), _email));
				break;
			}
			case Constants.NETWORK_SERVICE_CODE_UPDATE_PIC:
			{
				marketplace.updateImage(getApplicationContext());
				break;
			}
			case Constants.NETWORK_SERVICE_CODE_DOWNLOAD_LARGE:
			{
				String _mobile = intent.getStringExtra(Constants.INTENT_CONTACT_NUMBER);
				String[] filename = database.getFileUrl(_mobile).split("/");
				
				ImageDownloadObject downloadObject = new ImageDownloadObject(_mobile, 
						Constants.IMAGE_SERVER_LARGE_SIZE+filename[filename.length-1], 0);
				List<ImageDownloadObject> list = new ArrayList<ImageDownloadObject>();
				list.add(downloadObject);
				ImageDownloader downloader = new ImageDownloader(getApplicationContext(),list, false);
				downloader.startDownload();
				break;
			}
			case Constants.NETWORK_SERVICE_CODE_UPDATE_BALANCE:
			{
				int amount = intent.getIntExtra(Constants.INTENT_UPDATE_BALANCE, 0);
				onReceiveResponse(Constants.NETWORK_SERVICE_CODE_UPDATE_BALANCE, marketplace.updateBalance(getApplicationContext(), amount));
				break;
			}
			default:
				break;
			}
		}
	}
	
	public void getGCMRegID(final Context context){
		AsyncTask< Void, Void, String> asyncTask = new AsyncTask<Void, Void, String>() {

			@Override
			protected String doInBackground(Void... params) {
				String regID="";
				try{
					GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
					regID = gcm.register(Constants.GCM_SENDER_ID);
					Log.d(Constants.DEBUG,"Received registration id:-"+regID);
				}
				catch(Exception e){
					e.printStackTrace();
				}
				return regID;
			}
			
			@Override
			protected void onPostExecute(String regID){
				MyAccountManager accountManager = new MyAccountManager(context);
				
				if(regID!=null && regID.length()>0){
					System.out.println("Asked server for reg id");
					accountManager.setHasGCMId(accountManager.setGCMId(regID));
				}
				onReceiveResponse(Constants.NETWORK_SERVICE_CODE_GET_GCM, "GCM ID");
			}
		};
		asyncTask.execute();
	}

	private void onReceiveResponse(int serviceCode, String response){
		Log.d("CALLED","CALLED");
		if(response==null){
			Log.d("NetworkServiceResponse: "+serviceCode, "null");
			return;
		}
		Log.d("NetworkServiceResponse: "+serviceCode, response);
		JSONObject jobj ;
		switch (serviceCode) {
		case Constants.NETWORK_SERVICE_CODE_TRANSACTION:
		{
			try{
				jobj = new JSONObject(response);
				int success = jobj.getInt(Constants.WEB_RESPONSE_SUCCESS);
				if(success==1){
					String tid = jobj.getString(Constants.WEB_RESPONSE_TRANSACTION_ID);
					int isRequsted = jobj.getInt("IS_REQUESTED");
					if(isRequsted==0){
						int amount = jobj.getInt("AMOUNT");
						int bal = accountManager.getBalance();
						accountManager.setBalance(bal-amount);		
					}
					database.updateTransactionStatus(tid, 1);
					Log.d("transaction status",database.updateTransactionStatus(tid, 1)+"");
				}
				else if(success==-1){
					String tid = jobj.getString(Constants.WEB_RESPONSE_TRANSACTION_ID);
					database.updateTransactionStatus(tid, -1);
					Log.d("transaction status",database.updateTransactionStatus(tid, -1)+"");
				}
				else if(success==0){
					String tid = jobj.getString(Constants.WEB_RESPONSE_TRANSACTION_ID);
					database.updateTransactionStatus(tid, -1);
					Log.d("transaction status",database.updateTransactionStatus(tid, -1)+"");
				}
			}
			catch(Exception e){
				e.printStackTrace();
				return;
			}
			break;
		}
		case Constants.NETWORK_SERVICE_CODE_GET_GCM:
		{	
			break;
		}
		case Constants.NETWORK_SERVICE_CODE_UPDATE_BALANCE:
		{
			try{
				jobj = new JSONObject(response);
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Log.d(Constants.DEBUG,"Balance Updated Successfully");
				}
				else{
					Log.d(Constants.DEBUG,"Balance update failed.");
				}
			}
			catch(Exception e){
				e.printStackTrace();
				return;
			}
			break;
		}
		case Constants.NETWORK_SERVICE_CODE_INVITE:
		{
			try{
				jobj =  new JSONObject(response);
				
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Log.d(Constants.DEBUG,"user invited successfully.");
					String _contact_number = jobj.getString(Constants.WEB_RESPONSE_CONTACT_NUMBER);
					database.updateInviteStatus(_contact_number, 1);
				}
				else{
					String _contact_number = jobj.getString(Constants.WEB_RESPONSE_CONTACT_NUMBER);
					database.updateInviteStatus(_contact_number, 0);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			break;
		}
		case Constants.NETWORK_SERVICE_CODE_UPDATE_NAME:
			try {
				jobj =  new JSONObject(response);
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Log.d(Constants.DEBUG,"name updated successfully!");
					String _firstname = jobj.getString(Constants.WEB_RESPONSE_FIRSTNAME);
					String _lastname = jobj.getString(Constants.WEB_RESPONSE_LASTNAME);
					accountManager.editParameter(MyAccountManager.SHARED_PREFERENCES_VARIABLE_FIRSTNAME, _firstname);
					accountManager.editParameter(MyAccountManager.SHARED_PREFERENCES_VARIABLE_LASTNAME, _lastname);
				}
				else{
					Log.d(Constants.DEBUG,"Oops SOme error Occurred");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.NETWORK_SERVICE_CODE_UPDATE_EMAIL:
			try {
				jobj =  new JSONObject(response);
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					Log.d(Constants.DEBUG,"email updated successfully!");
					String _email = jobj.getString(Constants.WEB_RESPONSE_EMAIL);
					accountManager.editParameter(MyAccountManager.SHARED_PREFERENCES_VARIABLE_EMAIL, _email);
				}
				else{
					Log.d(Constants.DEBUG,"Oops SOme error Occurred");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case Constants.NETWORK_SERVICE_CODE_REGISTER_GCM_ON_CIBOLA:
			try{
				jobj =  new JSONObject(response);
				if(jobj.getInt(Constants.WEB_RESPONSE_SUCCESS)==1){
					boolean output = accountManager.setIsGCMRegisteredOnCibola(true);
					System.out.println("GCM saved on server: "+output);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			break;
		default:
			Log.d(Constants.DEBUG,"response service code mismatch. serviceCode:"+serviceCode);
			break;
		}
	}
	
	@Override
	public void onRequestComplete(RequestType requestType, String response) {
		
	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
		
	}
	
}

