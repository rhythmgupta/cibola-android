
package cibola.co.in.NetworkManager.Services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.Interfaces.WebConnectionCallbacks;
import cibola.co.in.NetworkManager.Marketplace;
import cibola.co.in.NetworkManager.RequestType;
import cibola.co.in.others.ContactsManager;

public class ContactsService extends IntentService implements WebConnectionCallbacks{

	public static boolean isRunning = false;
	private ContactsManager contactManager;
	private MyAccountManager accountManager;
	
	public ContactsService(String name) {
		super(name);
	}

	public ContactsService(){
		super("ContactsService");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		contactManager = new ContactsManager(getApplicationContext());
		accountManager = new MyAccountManager(getApplicationContext());
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if(intent==null){
			Log.d(Constants.BUG, "Intent is null!! How? Who triggered this service?");
			return;
		}
		try{
			int isInit = intent.getIntExtra(Constants.CONTACTS_MANAGER_IS_INIT, 0);
			Marketplace marketplace = new Marketplace(null);
			if(isInit==1){
				onReceiveResponse(marketplace.signupStepFour(getApplicationContext(), contactManager.getInitContactList(this)));
			}
			else{
				onReceiveResponse(marketplace.updateContactList(getApplicationContext(), contactManager.getAllNativeContactsQuick()));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void onReceiveResponse(String response){
		if(response!=null){
			Log.d("ContactsService::onReceiveResponse",response);
			contactManager.handleResponse(getApplicationContext(), true, response);
		}
		else{
			//Common.showToast(getApplicationContext(), "Some error occurred");
			Log.d("ContactsService::onReceiveResponse","Unable to get the response. Please check the response code and make sure that you have working internet connection.");
		}
	}

	@Override
	public void onRequestComplete(RequestType requestType, String response) {
	}

	@Override
	public void onRequestError(RequestType requestType, String errorResponse) {
	}

}
