
package cibola.co.in.NetworkManager;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.Graphics.PhotoManager;
import cibola.co.in.SqliteManager.Database;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

public class ImageDownloader {

	private Context mContext;
	private boolean running;
	private boolean isBulk;
	private int completedDownloads;
	private List<ImageDownloadObject> filesToDownload;
	private PhotoManager photoManager;
	private Database database;
	
	public ImageDownloader(Context context, List<ImageDownloadObject> pFiles, boolean _isBulk){
		this.filesToDownload = pFiles;
		this.running = false;
		this.completedDownloads = 0;
		this.photoManager = new PhotoManager();
		this.isBulk = _isBulk;
		this.mContext = context;
		database = new Database(context);
	}
	
	public void startDownload(){
		if(filesToDownload!=null && filesToDownload.size()>0){
			Log.d(Constants.DEBUG,"starting download "+filesToDownload.size()+" files.");
			running = true;
			for(final ImageDownloadObject dataObject:filesToDownload){
				ImageRequest imageRequest = new ImageRequest(dataObject.getFileURL(), new Response.Listener<Bitmap>() {
					 
				    @Override
				    public void onResponse(Bitmap response) {
				    	Log.d("File Download Complete: "+dataObject.getMobile(),dataObject.getFileURL());
				         if(response!=null){
				        	 if(isBulk){
				        		 photoManager.saveImageToDisk(response, dataObject.getMobile()+".png", CompressFormat.PNG, ImageType.Medium);
				        		 database.updateProfilePicture(dataObject.getMobile(), dataObject.getFileID(), dataObject.getFileURL());
				        		 Common.deleteLargeImage(dataObject.getMobile());
				        	 }
				        	 else{
				        		 photoManager.saveImageToDisk(response, dataObject.getMobile()+".png", CompressFormat.PNG, ImageType.Large);
				        		 Intent intent = new Intent(Constants.INTENT_LARGE_IMAGE_DOWNLOAD_COMPLETE);
				        		 intent.putExtra(Constants.INTENT_CONTACT_NUMBER, dataObject.getMobile());
				        		 mContext.sendBroadcast(intent);
				        		 return;
				        	 }
				         }
				         else{
				        	 Log.d(Constants.DEBUG,"Getting null response on Image download");
				         }
				         completedDownloads++;
				         if(completedDownloads==filesToDownload.size()){
				        	 running = false;
				        	 Intent i1 = new Intent();
				            	i1.setAction(Constants.CONTACTS_UPDATE_COMPLETE_BROADCAST_ACTION);
				            	i1.putExtra(Constants.CONTACTS_UPDATE_COMPLETE_BROADCAST_MESSAGE, 1);
				            	mContext.sendBroadcast(i1);
				         }
				    }
				}, 0, 0, null, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Log.d(Constants.DEBUG,"Getting volley error:"+error.getMessage());
					}
				});
				Log.d("Downloading Image:- ", dataObject.getFileURL());
				AppController.getInstance().addToRequestQueue(imageRequest, "Downloading "+dataObject.getMobile()+".png");
			}
		}
	}

	public boolean isRunning() {
		return running;
	}




	
}
