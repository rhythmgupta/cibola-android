
package cibola.co.in.NetworkManager;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import cibola.co.in.General.Common;
import cibola.co.in.General.Constants;
import cibola.co.in.General.MyAccountManager;
import cibola.co.in.SqliteManager.Database;

public class Marketplace {

	private Activity activity;
	private MyAccountManager accountManager;
	public Marketplace(Activity act){
		activity = act;
		
	}

	private String getURL(RequestType requestType){
		String url = Constants.DOMAIN_NAME;
		switch (requestType) {
		case RequestTypeSignUpStepOne:
			url += "api/signup/1/";
			break;
		case RequestTypeSignUpStepTwo:
			url += "api/signup/2/";
			break;
		case RequestTypeSignUpStepThree:
			url += "api/signup/3/";
			break;
		case RequestTypeSignIn:
			url += "api/signin/";
			break;
		case RequestTypeInitContacts:
			url += "api/sync_contacts/";
			break;
		case RequestTypeUpdateContactsList:
			url += "api/sync_contacts/";
			break;
		case RequestTypeInviteContact:
			url += "api/invite/";
			break;
		case RequestTypeNewTransaction:
			url += "api/insert_transaction/";
			break;
		case RequestTypeRegisterGcmOnCibola:
			url += "api/register_gcm/";
			break;
		case RequestTypeUnregisterGcmOnCibola:
			url += "api/unregister_gcm/";
			break;
		case RequestTypeProfileUpdateBalance:
			url += "api/update_balance/";
			break;
		case RequestTypeProfileUpdateEmail:
			url += "api/update_email/";
			break;
		case RequestTypeProfileUpdateName:
			url += "api/update_name/";
			break;
		case RequestTypeProfileUpdatePic:
			url += "api/update_pic/";
			break;
		case RequestTypeProfileUpdatePassword:
			url += "api/update_password/";
			break;
		case RequestTypeProfileUpdateSecurityQues:
			url += "api/update_security/";
			break;
		default:
			break;
		}
		Log.d("EXECUTING URL", url);
		return url;
	}

	public String updateContactList(Context context, String input){
		try {
			Request request = new Request(activity, getURL(RequestType.RequestTypeUpdateContactsList), 
					RequestType.RequestTypeUpdateContactsList, false);
			request.addParam(Constants.CONACTS_SYNC, input);
			request.setDoInAsyncTask(false);
			ServerConnector server = new ServerConnector(context, request);
			return server.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	//name, email and mobile number
	public void signupStepOne(Context context,String firstname, String lastname, String email, String mobile){
		Request req = new Request(activity, getURL(RequestType.RequestTypeSignUpStepOne), RequestType.RequestTypeSignUpStepOne,true);
		req.addParam("first_name", firstname);
		req.addParam("last_name", lastname);
		req.addParam("email", email);
		req.addParam("mobile", mobile);
		ServerConnector server = new ServerConnector(req, context, "Please wait...", true);
		server.start();
	}

	//verification code
	public void signupStepTwo(Context context,String mobile, int mobileCode){
		Request req = new Request(activity, getURL(RequestType.RequestTypeSignUpStepTwo),
				RequestType.RequestTypeSignUpStepTwo,true);
		req.addParam(Constants.WEB_USERNAME, mobile);
		req.addParam(Constants.WEB_VERIFICATION_CODE, mobileCode+"");
		ServerConnector server = new ServerConnector(req, context, "Processing...", true);
		server.start();
	}

	//Profile pic, gender and DOB
	public void signupStepThree(Context context, String username, String gender, String dob, String fileURI){
		File file = new File(fileURI);
		if(file.exists()){
			Log.d("SignupStepThree:FileExists", username+" "+gender+" "+dob+" "+fileURI);
			Request req = new Request(activity, getURL(RequestType.RequestTypeSignUpStepThree),
					RequestType.RequestTypeSignUpStepThree, fileURI,true);
			req.addParam(Constants.WEB_SIGNUP_GENDER, gender);
			req.addParam(Constants.WEB_SIGNUP_DOB, dob);
			req.addParam(Constants.WEB_USERNAME, username);
			FormUpload formUpload = new FormUpload(req, context, "We are almost done..", true);
			formUpload.start();
		}
		else{
			Log.d("SignupStepThree:File Doesn't exists", username+" "+gender+" "+dob+" "+fileURI);
			Request req = new Request(activity, getURL(RequestType.RequestTypeSignUpStepThree), 
					RequestType.RequestTypeSignUpStepThree,true);
			req.addParam(Constants.WEB_SIGNUP_GENDER, gender);
			req.addParam(Constants.WEB_SIGNUP_DOB, dob);
			req.addParam(Constants.WEB_USERNAME, username);
			ServerConnector server = new ServerConnector(req, context, "We are almost done..", true);
			server.start();
		}
	}

	//contacts initialization
	public String signupStepFour(Context context, String jsonContacts){
		Request request = new Request(activity, getURL(RequestType.RequestTypeInitContacts), RequestType.RequestTypeInitContacts, false);
		request.addParam(Constants.CONACTS_SYNC, jsonContacts);
		request.setDoInAsyncTask(false);
		ServerConnector server = new ServerConnector(context, request);
		return server.start();
	}

	public void signin(Context context, String username, String pwd){
		Request req = new Request(activity, getURL(RequestType.RequestTypeSignIn),
				RequestType.RequestTypeSignIn,true);
		req.addParam(Constants.WEB_LOGIN_USERNAME, username);
		req.addParam(Constants.WEB_LOGIN_PASSWORD, pwd);
		ServerConnector server = new ServerConnector(req, context, "Validating credentials...", true);
		server.start();

	}

	public String registerGCM(Context context, String regID ){
		Request request = new Request(activity, getURL(RequestType.RequestTypeRegisterGcmOnCibola), 
				RequestType.RequestTypeRegisterGcmOnCibola, false);
		accountManager = new MyAccountManager(context);
		String _username = accountManager.getUsername();
		if(_username!=null && _username.length()>1){
			request.setDoInAsyncTask(false);
			request.addParam(Constants.WEB_USERNAME, _username);
			request.addParam(Constants.WEB_GCM_REG_ID, regID);
			ServerConnector server = new ServerConnector(context, request);
			return server.start();
		}
		else{
			Log.d(Constants.BUG,"Marketplace:registerGCM - Uh Oh! Hell! Where is the username?");
			return null;
		}
	}

	public String unregisterGCM(Context context, String regID ){
		Request request = new Request(activity, getURL(RequestType.RequestTypeUnregisterGcmOnCibola), 
				RequestType.RequestTypeUnregisterGcmOnCibola, true);
		request.setDoInAsyncTask(false);
		request.addParam(Constants.WEB_GCM_REG_ID, regID);
		accountManager = new MyAccountManager(context);
		String _username = accountManager.getUsername();
		if(_username!=null && _username.length()>1){
			request.addParam(Constants.WEB_USERNAME, _username);
			ServerConnector server = new ServerConnector(context, request);
			return server.start();
		}
		else{
			Log.d(Constants.BUG,"Marketplace:registerGCM - Uh Oh! Hell! Where is the username?");
			return null;
		}
	}

	public String execNewTransaction(Context context, String transaction_id, String toUsername, String comment, 
			int amount, boolean isRequested){
		Database database = new Database(context);
		Request request = new Request(activity,	getURL(RequestType.RequestTypeNewTransaction), 
				RequestType.RequestTypeNewTransaction, false);

		request.addParam(Constants.WEB_TRANSACTION_ID, transaction_id);
		request.addParam(Constants.WEB_TRANSACTION_AMOUNT, amount+"");
		request.addParam(Constants.WEB_TRANSACTION_TO_USERNAME, toUsername);

		MyAccountManager accountManager = new MyAccountManager(context);
		String fromUsername = accountManager.getUsername();
		if(fromUsername!=null){
			request.addParam(Constants.WEB_TRANSACTION_FROM_USERNAME, fromUsername);	
		}
		else{
			Log.d(Constants.BUG,"Uh Oh! Where is the hell is the username?. Marketplace:execNewTransaction");
		}
		if(isRequested){
			request.addParam(Constants.WEB_TRANSACTION_IS_REQUESTED, "1");
		}
		else{
			request.addParam(Constants.WEB_TRANSACTION_IS_REQUESTED, "0");
		}

		if(comment==null || comment.length()==0){
			request.addParam(Constants.WEB_TRANSACTION_COMMENT, "");
			comment=null;
			Log.d(Constants.DEBUG,"Comment is null");
		}
		else{
			request.addParam(Constants.WEB_TRANSACTION_COMMENT, comment);
		}

		if(isRequested){
			database.addNewTransaction(transaction_id, toUsername, 1, 1, amount, comment, 0);
		}
		else{
			database.addNewTransaction(transaction_id, toUsername, 1, 0, amount, comment, 0);
		}
		request.setDoInAsyncTask(false);
		ServerConnector server =  new ServerConnector(context, request);
		return server.start();
	}


	public String invite(Context context, String mobilenumber){
		Request request = new Request(activity, getURL(RequestType.RequestTypeInviteContact), 
				RequestType.RequestTypeInviteContact, false);
		accountManager = new MyAccountManager(context);
		String _username = accountManager.getUsername();
		if(_username!=null && _username.length()>1){
			request.setDoInAsyncTask(false);
			request.addParam(Constants.WEB_USERNAME, _username);
			request.addParam(Constants.WEB_CONTACTS_INVITE, mobilenumber);
			ServerConnector server = new ServerConnector(context, request);
			return server.start();
		}
		else{
			Log.d(Constants.BUG,"Marketplace:invite - Uh Oh! Hell! Where is the username?");
			return null;
		}
	}
	
	
	public String updateName(Context context, String firstname, String lastname){
		Request request = new Request(activity, getURL(RequestType.RequestTypeProfileUpdateName),
				RequestType.RequestTypeProfileUpdateName, false);
		accountManager = new MyAccountManager(context);
		String _username = accountManager.getUsername();
		if(_username!=null && _username.length()>1){
			request.setDoInAsyncTask(false);
			request.addParam(Constants.WEB_USERNAME,  _username);
			request.addParam(Constants.WEB_UPDATE_FIRST_NAME, firstname);
			request.addParam(Constants.WEB_UPDATE_LAST_NAME, lastname);
			ServerConnector server = new ServerConnector(context, request);
			return server.start();
		}
		else{
			Log.d(Constants.BUG,"Marketplace:invite - Uh Oh! Hell! Where is the username?");
			return null;
		}
	}
	
	public String updateEmail(Context context, String email){
		Request request = new Request(activity, getURL(RequestType.RequestTypeProfileUpdateEmail),
				RequestType.RequestTypeProfileUpdateEmail, false);
		accountManager = new MyAccountManager(context);
		String _username = accountManager.getUsername();
		if(_username!=null && _username.length()>1){
			request.setDoInAsyncTask(false);
			request.addParam(Constants.WEB_USERNAME,  _username);
			request.addParam(Constants.WEB_UPDATE_EMAIL, email);
			ServerConnector server = new ServerConnector(context, request);
			return server.start();
		}
		else{
			Log.d(Constants.BUG,"Marketplace:invite - Uh Oh! Hell! Where is the username?");
			return null;
		}
	}
	
	public void updatePassword(Context context, String oldPass, String newPass){
		Request request = new Request(activity, getURL(RequestType.RequestTypeProfileUpdatePassword),
				RequestType.RequestTypeProfileUpdatePassword, true);
		accountManager = new MyAccountManager(context);
		String _username = accountManager.getUsername();
		if(_username!=null && _username.length()>1){
			request.setDoInAsyncTask(true);
			request.addParam(Constants.WEB_USERNAME,  _username);
			request.addParam(Constants.WEB_UPDATE_OLD_PWD, oldPass);
			request.addParam(Constants.WEB_UPDATE_NEW_PWD, newPass);
			ServerConnector server = new ServerConnector(request, context, "Hold on a second. We are update your password...", true);
			server.start();
		}
		else{
			Log.d(Constants.BUG,"Marketplace:invite - Uh Oh! Hell! Where is the username?");
		}
	}

	public void updateImage(Context context){
		String filePath = Common.getContactImagePath(null, ImageType.Self);
		Request req = new Request(activity, getURL(RequestType.RequestTypeProfileUpdatePic),
				RequestType.RequestTypeProfileUpdatePic, filePath,true);
		req.setDoInAsyncTask(false);
		accountManager  = new MyAccountManager(context);
		String _username = accountManager.getUsername();
		if(_username!=null && _username.length()>1){
			req.addParam(Constants.WEB_USERNAME, _username);
			
			FormUpload formUpload = new FormUpload(req, context, "We are almost done..", true);
			formUpload.start();
		}				
	}
	
	public String updateBalance(Context context, int amount){
		Request request = new Request(activity, getURL(RequestType.RequestTypeProfileUpdateBalance),
				RequestType.RequestTypeProfileUpdateBalance, false);
		accountManager = new MyAccountManager(context);
		String _username = accountManager.getUsername();
		if(_username!=null && _username.length()>1){
			request.setDoInAsyncTask(false);
			request.addParam(Constants.WEB_USERNAME,  _username);
			request.addParam(Constants.WEB_UPDATE_BALANCE, String.valueOf(amount));
			ServerConnector server = new ServerConnector(context, request);
			return server.start();
		}
		else{
			Log.d(Constants.BUG,"Marketplace:update balance - Uh Oh! Hell! Where is the username?");
			return null;
		}
	}

}
