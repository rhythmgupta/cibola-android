
package cibola.co.in.NetworkManager;

public class ImageDownloadObject {

	private String mobile;
	private int fileID;
	private String fileURL;
	
	public ImageDownloadObject(String pMobile, String pURL, int fileID){
		this.mobile = pMobile;
		this.fileID = fileID;
		this.fileURL = pURL;
	}
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getFileID() {
		return fileID;
	}
	public void setFileID(int fileID) {
		this.fileID = fileID;
	}
	public String getFileURL() {
		return fileURL;
	}
	public void setFileURL(String fileURL) {
		this.fileURL = fileURL;
	}
	
}
