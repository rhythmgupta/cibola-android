
package cibola.co.in.others;

import java.util.HashMap;


public class PhoneContactPair {

	private HashMap<String, String> hmap;
	private String jsonStringNumbers;
	
	public PhoneContactPair(HashMap<String, String> h, String s){
		this.setHmap(h);
		this.setJsonStringNumbers(s);
	}

	public HashMap<String, String> getHmap() {
		return hmap;
	}

	public void setHmap(HashMap<String, String> hmap) {
		this.hmap = hmap;
	}

	public String getJsonStringNumbers() {
		return jsonStringNumbers;
	}

	public void setJsonStringNumbers(String jsonStringNumbers) {
		this.jsonStringNumbers = jsonStringNumbers;
	}
	
}
