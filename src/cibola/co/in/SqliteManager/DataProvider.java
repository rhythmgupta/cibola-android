package cibola.co.in.SqliteManager;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

public class DataProvider extends ContentProvider{
	private DatabaseManager db ;

	private static final String AUTHORITY = "cibola.co.in.dataprovider";

	public static final Uri URI_CONTACTS = Uri.parse("content://"+AUTHORITY+"/"+Tables.TABLE_CONTACT);
	private static final int T_CONTACTS_ALL = 0;
	private static final int T_CONTACTS_SINGLE = 1;

	public static final Uri URI_TRANSACTIONS = Uri.parse("content://"+AUTHORITY+"/"+Tables.TABLE_TRANSACTIONS);
	private static final int T_TRANSACTIONS_ALL = 2;
	private static final int T_TRANSACTIONS_SINGLE =3;

	public static final Uri URI_RAWUPDATES = Uri.parse("content://"+AUTHORITY+"/"+Tables.TABLE_RAWUPDATES);
	private static final int T_RAWUPDATES_ALL = 4;
	private static final int T_RAWUPDATES_SINGLE =5;
	
	public static final Uri URI_BANK_ACCOUNTS = Uri.parse("content://"+AUTHORITY+"/"+Tables.TABLE_BANK_ACCOUNTS);
	private static final int T_BANKACCOUNT_ALL = 6;
	private static final int T_BANKACCOUNT_SINGLE =7;
	
	
	private static final UriMatcher uriMatcher;

	static{
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY, Tables.TABLE_CONTACT, T_CONTACTS_ALL);
		uriMatcher.addURI(AUTHORITY, Tables.TABLE_CONTACT+"/#", T_CONTACTS_SINGLE);
		uriMatcher.addURI(AUTHORITY, Tables.TABLE_TRANSACTIONS, T_TRANSACTIONS_ALL);
		uriMatcher.addURI(AUTHORITY, Tables.TABLE_TRANSACTIONS+"/#", T_TRANSACTIONS_SINGLE);
		uriMatcher.addURI(AUTHORITY, Tables.TABLE_RAWUPDATES, T_RAWUPDATES_ALL);
		uriMatcher.addURI(AUTHORITY, Tables.TABLE_RAWUPDATES+"/#", T_RAWUPDATES_SINGLE);
		uriMatcher.addURI(AUTHORITY, Tables.TABLE_BANK_ACCOUNTS, T_BANKACCOUNT_ALL);
		uriMatcher.addURI(AUTHORITY, Tables.TABLE_BANK_ACCOUNTS+"/#", T_BANKACCOUNT_SINGLE);
		
	}



	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int rowsAffected;

		switch(uriMatcher.match(uri)){
		case T_CONTACTS_ALL:
			rowsAffected =  db.delete(Tables.TABLE_CONTACT, selection, selectionArgs);
			break;
		case T_BANKACCOUNT_ALL:
			rowsAffected =  db.delete(Tables.TABLE_BANK_ACCOUNTS, selection, selectionArgs);
			break;
		case T_RAWUPDATES_ALL:
			rowsAffected =  db.delete(Tables.TABLE_RAWUPDATES, selection, selectionArgs);
			break;
		case T_TRANSACTIONS_ALL:
			rowsAffected =  db.delete(Tables.TABLE_TRANSACTIONS, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
		if(rowsAffected>0)
			getContext().getContentResolver().notifyChange(uri,null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long rowID = -1;
		switch(uriMatcher.match(uri))
		{
		case T_CONTACTS_ALL:
		{
			rowID = db.insertQuery(Tables.TABLE_CONTACT, values);
			break;
		}
		case T_TRANSACTIONS_ALL:
		{
			rowID = db.insertQuery(Tables.TABLE_TRANSACTIONS, values);
			break;
		}
		case T_RAWUPDATES_ALL:
		{
			rowID = db.insertQuery(Tables.TABLE_RAWUPDATES, values);
			break;
		}
		case T_BANKACCOUNT_ALL:
		{
			rowID = db.insertQuery(Tables.TABLE_BANK_ACCOUNTS, values);
			break;
		}
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri+" URI NO.: "+uriMatcher.match(uri));
		}
		if(rowID ==-1)
		{
			return null;
		}
		Uri retURI = ContentUris.withAppendedId(uri, rowID);
//		Log.d("Dataprovider insertion sucessful: retURI", retURI.toString());
		getContext().getContentResolver().notifyChange(retURI,null);
		return retURI;
	}

	@Override
	public boolean onCreate() {
		db = new DatabaseManager(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		//db = new DatabaseManager(getContext());
		Cursor cursor;

//		Log.i("URI", ""+uriMatcher.match(uri));
		switch(uriMatcher.match(uri)){
		case T_CONTACTS_ALL:
			cursor =  db.query(Tables.TABLE_CONTACT, projection, selection, selectionArgs, sortOrder);
			break;
		case T_TRANSACTIONS_ALL:
			cursor =  db.query(Tables.TABLE_TRANSACTIONS, projection, selection, selectionArgs, sortOrder);
			break;
		case T_BANKACCOUNT_ALL:
			cursor =  db.query(Tables.TABLE_BANK_ACCOUNTS, projection, selection, selectionArgs, sortOrder);
			break;
		case T_RAWUPDATES_ALL:
			cursor =  db.query(Tables.TABLE_RAWUPDATES, projection, selection, selectionArgs, sortOrder);
			break;
		
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
		cursor.setNotificationUri(getContext().getContentResolver(), uri);

		return cursor;

	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int rowsAffected;

		switch(uriMatcher.match(uri)){
		case T_CONTACTS_ALL:
			rowsAffected =  db.updateQuery(Tables.TABLE_CONTACT, values, selection, selectionArgs);
			break;
		case T_BANKACCOUNT_ALL:
			rowsAffected =  db.updateQuery(Tables.TABLE_BANK_ACCOUNTS, values, selection, selectionArgs);
			break;
		case T_TRANSACTIONS_ALL:
			rowsAffected =  db.updateQuery(Tables.TABLE_TRANSACTIONS, values, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
		if(rowsAffected>0){
			getContext().getContentResolver().notifyChange(uri,null);
		}
		return rowsAffected;
	}

}
