
package cibola.co.in.SqliteManager;

import android.database.sqlite.SQLiteDatabase;

public class Tables {

	/* User table 
	 * 2 things must be entered while insertion i.e, they can't be null
	 * 	- contact_name
	 * 	- contact_id
	 * */
	public static final String TABLE_CONTACT = "contacts";
	public static final String contact_serial = "_id";
	public static final String contact_mobile = "uid";
	public static final String contact_name = "name";
	public static final String contact_email = "email";
	public static final String contact_is_blocked = "blocked"; // 0=> false | 1 => true
	public static final String contact_timestamp = "timestamp"; // latest timestamp
	public static final String contact_is_invited = "invited"; // 1=> yes and 0=> no
	public static final String contact_unread_transaction_count = "new_trans_count" ;
	public static final String contact_total_transaction_count = "total_trans_count" ;
	public static final String contact_image_id = "file_id";
	public static final String contact_image_local_filepath = "filepath";
	public static final String contact_to_from = "to_from"; // 0 => to, 1=> from
	public static final String contact_send_req = "send_req";// 0 => sent, 1=> Requested
	public static final String contact_amount = "amount";
	public static final String contact_comment = "comment";
	public static final String contact_background = "bg_color";
	public static final String contact_onCibola = "on_cibola";
	public static final String CREATE_TABLE_CONTACTS = " create table if not exists "+TABLE_CONTACT+" ( "+
			contact_serial + " integer primary key autoincrement not null, "+
			contact_mobile + " integer not null unique, "+ // mobile number
			contact_name + " text, "+ // User's Full Name
			contact_email + " text, "+ // User's email
			contact_is_blocked + " integer default(0), "+ // is_blocked
			contact_is_invited + " integer default(0), "+ // is user invited
			contact_image_id + " integer default(-1), "+ // profile picture id
			contact_image_local_filepath + " text, "+ // profile picture filepath
			contact_to_from+" integer default(0), "+
			contact_send_req+" integer default(0), "+
			contact_amount+" integer default(0), "+
			contact_comment+ " text, "+
			contact_background+" integer default(0), "+
			contact_onCibola+" integer default(0), "+
			contact_unread_transaction_count +" integer default(0), "+ // unread transactions
			contact_total_transaction_count +" integer default(0), "+ // unread transactions
			contact_timestamp + " integer default(0) "+ // timestamp in ms
			//" unique( "+Tables.contact_serial+" , "+Tables.contact_id+") "+
			" ); ";
	//	public static final String[] table1_user_column_array = {contact_serial,contact_id,contact_name,contact_sorting_var,contact_new_transaction_count};


	/* transaction table
	 *  1. while insertion 6 things must not be null. 
	 *  	- opponent_id
	 *  	- to_from
	 *  	- send_req
	 *  	- amount
	 *  	- timestamp
	 *  	- transaction_id // Not sure about it.
	 */

	public static final String TABLE_TRANSACTIONS = "transactions";
	public static final String trans_serial = "_id";
	public static final String trans_transaction_id = "tid";
	public static final String trans_opponent_id = "opp_id";
	public static final String trans_to_from = "to_from"; // 0 => to, 1=> from me
	public static final String trans_send_req = "send_req";// 0 => sent, 1=> Requested
	public static final String trans_amount = "amount";
	public static final String trans_comment = "comment";
	public static final String trans_timestamp = "timestamp"; // in milliseconds 
	public static final String trans_status = "status"; // status = 0 <-> waiting, status=1 <->sent to server, status =2 <-> delivered to other user, status = -1 <-> failed.
	public static final String trans_isDate = "isDate"; // 1 => date row
	public static final String CREATE_TABLE_TRANSACTIONS = " create table if not exists "+TABLE_TRANSACTIONS+" ( "+
			trans_serial+" integer primary key autoincrement not null, "+
			trans_transaction_id+" text unique, "+ // NOT FIXED YET, WHETHER TO KEEP NULL OR NOT
			trans_opponent_id+" integer not null, "+
			trans_to_from+" integer not null, "+
			trans_send_req+" integer not null, "+
			trans_amount+" integer not null, "+
			trans_comment+ " text, "+
			trans_timestamp+ " integer not null , "+
			trans_status +" integer default(0), "+
			trans_isDate +" integer default(0) "+
			" ); ";


	/*
	 * RawUpdates
	 *  -id
	 *  -mobile
	 *  -name
	 *  -email
	 *  -ack
	 */
	public static final String TABLE_RAWUPDATES = "raw_updates";
	public static final String rawupdates_serial = "_id";
	public static final String rawupdates_mobile = "mobile";
	public static final String rawupdates_name = "name";
	public static final String rawupdates_email = "email";
	public static final String rawupdates_ack = "ack";
	public static final String rawupdates_timestamp = "timestamp"; // latest timestamp
	public static final String CREATE_TABLE_RAWUPDATES = " create table if not exists "+TABLE_RAWUPDATES+" ( "+
			rawupdates_serial + " integer primary key autoincrement not null, "+
			rawupdates_mobile + " integer not null unique, "+ // mobile number
			rawupdates_name + " text, "+ // User's Full Name
			rawupdates_email + " text, "+ // email
			rawupdates_ack + " integer default(0), "+ // ack
			rawupdates_timestamp + " integer default(0) "+ // timestamp in ms
			//" unique( "+Tables.contact_serial+" , "+Tables.contact_id+") "+
			" ); ";

	
	public static final String TABLE_BANK_ACCOUNTS = "accounts";
	public static final String account_serial = "_id";
	public static final String account_name = "account_name";
	public static final String account_number = "account_no";
	public static final String account_ifsc = "ifsc";
	public static final String account_timestamp = "timestamp";
	public static final String CREATE_TABLE_BANK_ACCOUNTS = " create table if not exists "+TABLE_BANK_ACCOUNTS+" ( "+
			account_serial + " integer primary key autoincrement not null, "+
			account_number + " integer not null unique, "+ // account number
			account_name + " text, "+ // account holder name
			account_ifsc + " text, "+ // Bank IFSC code
			account_timestamp + " integer default(0) "+ // timestamp in ms
			" ); ";
	


	/*
	 * Invoices Table
	 * 		- _id
	 * 		- invoice_id
	 * 		- merchant_name
	 * 		- merchant_id
	 * 		- timestamp
	 * 		- amount
	 * 		- status
	 * 		- isDate
	 */

	public static final String TABLE_INVOICES = "invoices";
	public static final String invoice_serial = "_id";
	public static final String invoice_id = "invoice_id";
	public static final String invoice_merchant_name = "merchant_name";
	public static final String invoice_merchant_id = "merchant_id";
	public static final String invoice_timestamp ="timestamp";
	public static final String invoice_items = "items";
	public static final String invoice_amount = "amount";
	public static final String invoice_status = "status";
	public static final String invoice_isDate = "isDate";
	public static final String CREATE_TABLE_INVOICES = " create table if not exists "+TABLE_INVOICES+" ( "+
			invoice_serial+" integer primary key autoincrement not null, "+
			invoice_id+" text, "+
			invoice_merchant_name+" text, "+
			invoice_merchant_id+" text, "+
			invoice_amount+" integer not null, "+
			invoice_items+" text, "+
			invoice_timestamp+" integer not null, "+
			invoice_status+" integer default(0), "+
			invoice_isDate+" integer default(0) "+
			" ) ";


	/* Variables table*/
	public static final String TABLE_INT_VARIABLES = "int_variables";
	public static final String var_serial = "_id";
	public static final String var_name = "var_name";
	public static final String var_value = "var_value";
	public static final String CREATE_TABLE_VARIABLES  = " create table if not exists "+TABLE_INT_VARIABLES+" ( "+ 
			var_serial+" integer primary key autoincrement not null, "+
			var_name+" text, "+
			var_value+" integer not null, "+
			" unique( "+var_serial+","+var_name+") "+
			" ); ";

	/*
	 * Recharge Plans Table
	 * 		- _id
	 * 		- operator_name
	 * 		- tab_id => 1: Top Ups, 2: 2G/3G, 3: Message, 4: Specials
	 * 		- price
	 * 		- detail
	 * 		- validity
	 */

	public static final String TABLE_RECHARGE_PLANS = "recharge_plans";
	public static final String rplans_serial = "_id";
	public static final String rplans_operator_name = "operator_name";
	public static final String rplans_tab_id = "tab_id";
	public static final String rplans_price = "price";
	public static final String rplans_details ="details";
	public static final String rplans_validity = "validity";
	public static final String CREATE_TABLE_RECHARGE_PLANS = " create table if not exists "+TABLE_RECHARGE_PLANS+" ( "+
			rplans_serial+" integer primary key autoincrement not null, "+
			rplans_operator_name+" text, "+
			rplans_tab_id+" integer not null, "+
			rplans_price+" integer not null, "+
			rplans_details+" text, "+
			rplans_validity+" text "+
			" ) ";




	/*-- creating chat tables-- */
	public static void createTables(SQLiteDatabase database){
		database.execSQL(CREATE_TABLE_CONTACTS);
		database.execSQL(CREATE_TABLE_TRANSACTIONS);
		database.execSQL(CREATE_TABLE_RAWUPDATES);
		database.execSQL(CREATE_TABLE_BANK_ACCOUNTS);
		
		//		database.execSQL(CREATE_TABLE_VARIABLES);
		//		database.execSQL(CREATE_TABLE_INVOICES);
		//		database.execSQL(CREATE_TABLE_RECHARGE_PLANS);
	}

	public static void dropTables(SQLiteDatabase database){
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTIONS);
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_RAWUPDATES);
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_BANK_ACCOUNTS);
		//		database.execSQL("DROP TABLE IF EXISTS " + TABLE_INT_VARIABLES);
		//		database.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICES);
		//		database.execSQL("DROP TABLE IF EXISTS "+ TABLE_RECHARGE_PLANS);
	}



}
