/**
 * @author Rhythm Gupta
 * 
 * This package contains classes that are helpful in managing/reading/writing local database.
 * Tables.java - Defines structure of database tables.
 */
package cibola.co.in.SqliteManager;