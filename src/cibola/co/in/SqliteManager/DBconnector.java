
package cibola.co.in.SqliteManager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBconnector extends SQLiteOpenHelper{

	public static final String PRIMARY_DATABASE_NAME="cibola.dynamic.db";
	public static final int DATABASE_VERSION = 1;
	
	public DBconnector(Context context) {
		super(context, PRIMARY_DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Tables.createTables(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Tables.dropTables(db);
		onCreate(db);
	}
	

}

